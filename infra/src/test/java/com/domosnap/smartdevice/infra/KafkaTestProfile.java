package com.domosnap.smartdevice.infra;

import io.quarkus.test.junit.QuarkusTestProfile;

import java.util.HashMap;
import java.util.Map;

public class KafkaTestProfile implements QuarkusTestProfile {

    @Override
    public Map<String, String> getConfigOverrides() {
        Map<String, String> configuration = new HashMap<>();

        configuration.put("event.store.class", "com.domosnap.tools.core.services.eventstore.KafkaEventStore");
        configuration.put("kafka.topic.main", "test-smartdevice");
        configuration.put("kafka.topic.repo", "test-smartdevice-directory");

        return configuration;
    }
}
package com.domosnap.smartdevice.infra;


import io.github.embeddedkafka.EmbeddedKafka;
import io.github.embeddedkafka.EmbeddedKafkaConfig;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.restassured.http.ContentType;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.*;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;
import static io.restassured.RestAssured.given;
import static org.awaitility.Awaitility.await;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import java.util.*;
import java.util.Arrays;

@QuarkusTest
@TestProfile(KafkaTestProfile.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class KafkaSmartDeviceTest {

    private static final Logger logger = Logger.getLogger(KafkaSmartDeviceTest.class.getName());

    private static final String ATTRIBUTE_SMART_DEVICE_ID = "id";
    private static final String ATTRIBUTE_URI = "uri";
    private static final String ATTRIBUTE_TYPE = "type";
    private static final String ATTRIBUTE_MEASURES = "measures";
    private static final String ATTRIBUTE_ACTIVE = "active";
    private static final String ATTRIBUTE_DELETED = "deleted";

    private static final String ATTRIBUTE_DESCRIPTION = "description";

    private static final String ATTRIBUTE_KEY = "key";

    private final String jwt = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik9saXZpZXIgRHJpZXNiYWNoIiwiaWF0IjoxNTE2MjM5MDIyLCJleHAiOjIwMTYyMzkwMjIsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9yZWFsbXMvbWFzdGVyIiwiYXVkIjoic21hcnRkZXZpY2UtcmVzdCIsInVzZXJfaWQiOiJuNU1IRzJUTHVxWWJ6YzhBcmg2THdWTGZpWnUyIiwiZW1haWwiOiJvbGl2aWVyLmRyaWVzYmFjaEBjdXJhbnRpLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlfQ.DzUZ7R0Xk2A6nAwa3rM5WOk804YNd8Tjvo3yBYjBQUxlMb8vusTlMD07pJjahg3zMXgtofogvg_F2TiAl67dKUVyxUeebUvTBvxZlg6tI5OSS-0mKvs4wX1fSk70H6hf9Axl3MuX0DCR--ln7VRGYTNqPsFY5MkPlWQ8Amv2RVdxxv6FIlEGrPmaXdSwJcB183H67hGIMktLVsPckx8lNuILwHKCaqZ3RPEf1YeqYivlkAWkkfRA-YQQPD8sX1-tRbWeL1AQe41NDe4r5xvTGwRc0Waetva1NHWReWxeYACBe7uH9_lgwjDXWrix0qWrUOGB3DfjulBtZUpRiQVI-Q";

    @ConfigProperty(name = "modedegrade", defaultValue="true")
    Boolean modedegrade;

    @BeforeAll
    static void setup() {
        try {

            EmbeddedKafkaConfig c = EmbeddedKafkaConfig.defaultConfig();
            EmbeddedKafka.start(c);

            TimeUnit.SECONDS.sleep(5);

            // Attendre que le broker soit prêt
            await()
                    .atMost(30, TimeUnit.SECONDS)
                    .pollInterval(1, TimeUnit.SECONDS)
                    .until(() -> {
                        try (AdminClient tempClient = AdminClient.create(
                                Collections.singletonMap("bootstrap.servers", "localhost:6001"))) {
                            // Créer les deux topics
                            tempClient.createTopics(Arrays.asList(
                                    new NewTopic("test-smartdevice", 1, (short)1),
                                    new NewTopic("test-smartdevice-directory", 1, (short)1)
                            )).all().get(5, TimeUnit.SECONDS);
                            return true;
                        } catch (Exception e) {
                            logger.warning("Erreur lors de la création des topics: " + e.getMessage());
                            return false;
                        }
                    });

        } catch (Exception e) {
            logger.severe("Failed to setup Kafka: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @AfterAll
    static void teardown() {
        try {
            EmbeddedKafka.stop();
        } catch (Exception e) {
            logger.severe("Error during cleanup: " + e.getMessage());
        }
    }

    @Test
    @DisplayName("Test Creation SmartDevice with Kafka")
    @Order(1)
    public void testCreateSmartDevice() {

        modedegrade = true ;

        String uid = UUID.randomUUID().toString();
        JsonObject Register1 = new JsonObject().put(ATTRIBUTE_KEY, "key1234").put(ATTRIBUTE_TYPE, "type1")
                .put(ATTRIBUTE_DESCRIPTION, "desc");
        JsonObject Register2 = new JsonObject().put(ATTRIBUTE_KEY, "key4321").put(ATTRIBUTE_TYPE, "type2")
                .put(ATTRIBUTE_DESCRIPTION, "desc2");
        JsonArray measuresList = new JsonArray().add(Register1).add(Register2);

        given()
                .auth().oauth2(jwt)
                .contentType(ContentType.JSON)
                .body(new JsonObject()
                        .put(ATTRIBUTE_SMART_DEVICE_ID, uid)
                        .put(ATTRIBUTE_URI, "scs://12345@192.168.1.35:20000")
                        .put(ATTRIBUTE_TYPE, "TEST")
                        .put(ATTRIBUTE_MEASURES, measuresList)
                        .put(ATTRIBUTE_ACTIVE, true).toString())
                .when().post("/devices")
                .then()
                .statusCode(201)
                .and()
                .body(ATTRIBUTE_SMART_DEVICE_ID, is(uid));

        await()
                .atMost(30, TimeUnit.SECONDS)
                .pollInterval(1, TimeUnit.SECONDS)
                .untilAsserted(() -> {
                    given()
                            .auth().oauth2(jwt)
                            .contentType(ContentType.JSON)
                            .when()
                            .get("/devices")
                            .then()
                            .statusCode(200)
                            .body("$.size()", equalTo(1));
                });

    }


    @Test
    @DisplayName("Test Update SmartDevice with kafka")
    @Order(2)
    public void testUpdateSmartDevice() {

        String uid3 = given()
                .auth().oauth2(jwt)
                .when().get("/devices")
                .then()
                .statusCode(200)
                .extract().jsonPath().getString("[0].id");

        // Met à jour avec l'ID récupéré
        given()
                .auth().oauth2(jwt)
                .contentType(ContentType.JSON)
                .body("{\"id\": \"" + uid3 + "\", \"uri\": \"scs://12345@192.168.1.35:20001\", \"type\": \"TEST2\", \"active\": true}")
                .when().put("/devices/" + uid3)
                .then()
                .statusCode(200)
                .body("id", equalTo(uid3))
                .body("uri", equalTo("scs://12345@192.168.1.35:20001"))
                .body("type", equalTo("TEST2"))
                .body("measures.size()", equalTo(2))
                .body("active", equalTo("true"))
                .body("deleted", is(false));

        await()
                .atMost(30, TimeUnit.SECONDS)
                .pollInterval(1, TimeUnit.SECONDS)
                .untilAsserted(() -> {
                    given()
                            .auth().oauth2(jwt)
                            .contentType(ContentType.JSON)
                            .when().get("/devices/" + uid3)
                            .then()
                            .statusCode(200)
                            .body("id", equalTo(uid3))
                            .body("uri", equalTo("scs://12345@192.168.1.35:20001"))
                            .body("type", equalTo("TEST2"))
                            .body("measures.size()", equalTo(2))
                            .body("active", equalTo("true"))
                            .body("deleted", is(false));
                });
    }


    @Test
    @DisplayName("Test Update SmartDevice with change measures with kafka")
    @Order(3)
    public void testUpdateSmartDevice_UpdateMeasure() {

        JsonObject newMeasure1 = new JsonObject().put(ATTRIBUTE_KEY, "key1234").put(ATTRIBUTE_TYPE, "type1update")
                .put(ATTRIBUTE_DESCRIPTION, "descupdate");
        JsonObject newMeasure2 = new JsonObject().put(ATTRIBUTE_KEY, "key4321").put(ATTRIBUTE_TYPE, "type2update")
                .put(ATTRIBUTE_DESCRIPTION, "desc2update");
        JsonArray newMeasuresList = new JsonArray().add(newMeasure1).add(newMeasure2);


        String uid3 = given()
                .auth().oauth2(jwt)
                .when().get("/devices")
                .then()
                .statusCode(200)
                .extract().jsonPath().getString("[0].id");


        given()
                .auth().oauth2(jwt)
                .contentType(ContentType.JSON)
                .body("{\"id\": \"" + uid3 + "\", \"uri\": \"scs://12345@192.168.1.35:20001\", \"type\": \"TEST2\", \"active\": true , \"measures\": " + newMeasuresList + "}")
                .when().put("/devices/" + uid3)
                .then()
                .statusCode(200)
                .body("id", equalTo(uid3))
                .body("uri", equalTo("scs://12345@192.168.1.35:20001"))
                .body("type", equalTo("TEST2"))
                .body("measures.size()", equalTo(2))
                .body("active", equalTo("true"))
                .body("deleted", is(false));


        await()
                .atMost(30, TimeUnit.SECONDS)
                .pollInterval(1, TimeUnit.SECONDS)
                .untilAsserted(() -> {
                    given()
                            .auth().oauth2(jwt)
                            .contentType(ContentType.JSON)
                            .when().get("/devices/" + uid3)
                            .then()
                            .statusCode(200)
                            .body("id", equalTo(uid3))
                            .body("uri", equalTo("scs://12345@192.168.1.35:20001"))
                            .body("type", equalTo("TEST2"))
                            .body("measures.size()", equalTo(2))
                            .body("measures[0].key", equalTo("key1234"))
                            .body("measures[0].type", equalTo("type1update"))
                            .body("measures[0].description", equalTo("descupdate"))
                            .body("measures[1].key", equalTo("key4321"))
                            .body("measures[1].type", equalTo("type2update"))
                            .body("measures[1].description", equalTo("desc2update"))
                            .body("active", equalTo("true"))
                            .body("deleted", is(false));
                });
    }


    @Test
    @DisplayName("Test Update SmartDevice with create measures with Kafka")
    @Order(4)
    public void testUpdateSmartDevice_CreateMeasure() {

        // Update mesures
        JsonObject newMeasure1 = new JsonObject().put(ATTRIBUTE_KEY, "key1234").put(ATTRIBUTE_TYPE, "type1update")
                .put(ATTRIBUTE_DESCRIPTION, "descupdate");
        JsonObject newMeasure2 = new JsonObject().put(ATTRIBUTE_KEY, "key4321").put(ATTRIBUTE_TYPE, "type2update")
                .put(ATTRIBUTE_DESCRIPTION, "desc2update");
        JsonObject newMeasure3 = new JsonObject().put(ATTRIBUTE_KEY, "newKey123").put(ATTRIBUTE_TYPE, "newType")
                .put(ATTRIBUTE_DESCRIPTION, "newDescription");
        JsonArray newMeasuresList = new JsonArray().add(newMeasure1).add(newMeasure2).add(newMeasure3);

        String uid3 = given()
                .auth().oauth2(jwt)
                .when().get("/devices")
                .then()
                .statusCode(200)
                .extract().jsonPath().getString("[0].id");

        given()
                .auth().oauth2(jwt)
                .contentType(ContentType.JSON)
                .body("{\"id\": \"" + uid3 + "\", \"uri\": \"scs://12345@192.168.1.35:20001\", \"type\": \"TEST2\", \"active\": true , \"measures\": " + newMeasuresList + "}")
                .when().put("/devices/" + uid3)
                .then()
                .statusCode(200)
                .body("id", equalTo(uid3))
                .body("uri", equalTo("scs://12345@192.168.1.35:20001"))
                .body("type", equalTo("TEST2"))
                .body("measures.size()", equalTo(3))
                .body("active", equalTo("true"))
                .body("deleted", is(false));

        await()
                .atMost(30, TimeUnit.SECONDS)
                .pollInterval(1, TimeUnit.SECONDS)
                .untilAsserted(() -> {
                    given()
                            .auth().oauth2(jwt)
                            .contentType(ContentType.JSON)
                            .when().get("/devices/" + uid3)
                            .then()
                            .statusCode(200)
                            .body("id", equalTo(uid3))
                            .body("uri", equalTo("scs://12345@192.168.1.35:20001"))
                            .body("type", equalTo("TEST2"))
                            .body("measures.size()", equalTo(3))
                            .body("measures[0].key", equalTo("key1234"))
                            .body("measures[0].type", equalTo("type1update"))
                            .body("measures[0].description", equalTo("descupdate"))
                            .body("measures[1].key", equalTo("key4321"))
                            .body("measures[1].type", equalTo("type2update"))
                            .body("measures[1].description", equalTo("desc2update"))
                            .body("measures[2].key", equalTo("newKey123"))
                            .body("measures[2].type", equalTo("newType"))
                            .body("measures[2].description", equalTo("newDescription"))
                            .body("active", equalTo("true"))
                            .body("deleted", is(false));
                });

    }

    @Test
    @DisplayName("Test Update SmartDevice with delete measures")
    @Order(5)
    public void testUpdateSmartDevice_DeleteMeasure() {

        // Update mesures

        JsonObject newMeasure1 = new JsonObject().put(ATTRIBUTE_KEY, "key1234").put(ATTRIBUTE_TYPE, "type1update")
                .put(ATTRIBUTE_DESCRIPTION, "descupdate");
        JsonObject newMeasure2 = new JsonObject().put(ATTRIBUTE_KEY, "key4321").put(ATTRIBUTE_TYPE, "type2update")
                .put(ATTRIBUTE_DESCRIPTION, "desc2update");
        JsonObject newMeasure3 = new JsonObject().put(ATTRIBUTE_KEY, "newKey123").put(ATTRIBUTE_TYPE, "newType")
                .put(ATTRIBUTE_DESCRIPTION, "newDescription");
        JsonArray newMeasuresList = new JsonArray().add(newMeasure1).add(newMeasure2).add(newMeasure3);
        newMeasuresList.remove(newMeasure3);

        String uid3 = given()
                .auth().oauth2(jwt)
                .when().get("/devices")
                .then()
                .statusCode(200)
                .extract().jsonPath().getString("[0].id");

        given()
                .auth().oauth2(jwt)
                .contentType(ContentType.JSON)
                .body("{\"id\": \"" + uid3 + "\", \"uri\": \"scs://12345@192.168.1.35:20001\", \"type\": \"TEST2\", \"active\": true , \"measures\": " + newMeasuresList + "}")
                .when().put("/devices/" + uid3)
                .then()
                .statusCode(200)
                .body("id", equalTo(uid3))
                .body("uri", equalTo("scs://12345@192.168.1.35:20001"))
                .body("type", equalTo("TEST2"))
                .body("measures.size()", equalTo(2))
                .body("active", equalTo("true"))
                .body("deleted", is(false));

        await()
                .atMost(30, TimeUnit.SECONDS)
                .pollInterval(1, TimeUnit.SECONDS)
                .untilAsserted(() -> {
                    given()
                            .auth().oauth2(jwt)
                            .contentType(ContentType.JSON)
                            .when().get("/devices/" + uid3)
                            .then()
                            .statusCode(200)
                            .body("id", equalTo(uid3))
                            .body("uri", equalTo("scs://12345@192.168.1.35:20001"))
                            .body("type", equalTo("TEST2"))
                            .body("measures.size()", equalTo(2))
                            .body("measures[0].key", equalTo("key1234"))
                            .body("measures[0].type", equalTo("type1update"))
                            .body("measures[0].description", equalTo("descupdate"))
                            .body("measures[1].key", equalTo("key4321"))
                            .body("measures[1].type", equalTo("type2update"))
                            .body("measures[1].description", equalTo("desc2update"))
                            .body("active", equalTo("true"))
                            .body("deleted", is(false));
                });
    }


    @Test
    @DisplayName("Test Delete SmartDevice with kafka")
    @Order(6)
    public void testDeleteSmartDevice () {

        String uid4 = given()
                .auth().oauth2(jwt)
                .when().get("/devices")
                .then()
                .statusCode(200)
                .extract().jsonPath().getString("[0].id");

        given()
                .auth().oauth2(jwt)
                .contentType(ContentType.JSON)
                .when().delete("/devices/" + uid4)
                .then()
                .statusCode(204);

        given()
                .auth().oauth2(jwt)
                .contentType(ContentType.JSON)
                .when().get("/devices/" + uid4)
                .then()
                .statusCode(200)
                .and().body(ATTRIBUTE_DELETED, is(true));

        modedegrade = false ;
    }

}

//package com.domosnap.smartdevice.infra;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//
//import java.io.IOException;
//import java.util.UUID;
//
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.MethodOrderer;
//import org.junit.jupiter.api.Order;
//import org.junit.jupiter.api.RepeatedTest;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.TestMethodOrder;
//import org.junit.jupiter.api.extension.ExtendWith;
//
//import com.domosnap.smartdevice.infra.launcher.ServerSmartDevice;
//
//import io.vertx.core.DeploymentOptions;
//import io.vertx.core.Vertx;
//import io.vertx.core.http.HttpClient;
//import io.vertx.core.http.HttpClientResponse;
//import io.vertx.core.http.HttpMethod;
//import io.vertx.core.json.JsonArray;
//import io.vertx.core.json.JsonObject;
//import io.vertx.ext.web.client.WebClient;
//import io.vertx.junit5.Checkpoint;
//import io.vertx.junit5.VertxExtension;
//import io.vertx.junit5.VertxTestContext;
//
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
//@ExtendWith(VertxExtension.class)
//public class SmartDeviceAPITest {
//
////	private static final String HEADER_LOCATION = "Location";
//	private static final String HEADER_ID_PARTNER = "idPartner";
//	private static final String ATTRIBUTE_SMART_DEVICE_ID = "id";
//	private static final String ATTRIBUTE_ID_PARTNER = "idPartner";
//	private static final String ATTRIBUTE_URI = "uri";
//	private static final String ATTRIBUTE_TYPE = "type";
//	private static final String ATTRIBUTE_MEASURES = "measures";
//	private static final String ATTRIBUTE_ACTIVE = "active";
//	private static final String ATTRIBUTE_DELETED = "deleted";
//
//	private static final String ATTRIBUTE_DESCRIPTION = "description";
//
//	private static final String ATTRIBUTE_KEY = "key";
//
//	private String host = "localhost";
//	private int port = 8080;
////private String jwt ="eyJhbGciOiJSUzI1NiIsImtpZCI6ImFiZGY2YzhiY2NiNDk4NzAzZGZjOWRmYjgwNGI0ZTE2YmIyNDI1MzQiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiT2xpdmllciBEcmllc2JhY2giLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1CSVJmT3J5dThVYy9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBQS9BQ0hpM3JlSUVVd21VSldqbTNoWmlFRXZCNjlJb2N1OHN3L3M5Ni1jL3Bob3RvLmpwZyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9jdXJhbnRpLW12cCIsImF1ZCI6ImN1cmFudGktbXZwIiwiYXV0aF90aW1lIjoxNTYxNDk2NTQxLCJ1c2VyX2lkIjoibjVNSEcyVEx1cVliemM4QXJoNkx3VkxmaVp1MiIsInN1YiI6Im41TUhHMlRMdXFZYnpjOEFyaDZMd1ZMZmladTIiLCJpYXQiOjE1NjE0OTY1NDEsImV4cCI6MTU2MTUwMDE0MSwiZW1haWwiOiJvbGl2aWVyLmRyaWVzYmFjaEBjdXJhbnRpLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7Imdvb2dsZS5jb20iOlsiMTE1Mjc0NTI4ODU2NDY0NjQ0MDUyIl0sImVtYWlsIjpbIm9saXZpZXIuZHJpZXNiYWNoQGN1cmFudGkuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoiZ29vZ2xlLmNvbSJ9fQ.UurHAtR1EV-CR7YMpbhGmS0akgHCh4nIgV3cMQlo8GbnKBoQEKLW3gBx_PiFvxTghVRfmoKeyBw9lGt-0NZZ4ylYFyXBRWHeJYsThugmVuPQdDP2Ec4_FLcK15kGS6z2WYgK9ZFyVakQXrjUrMHe727NbN9LaaqVSzq3Il51gm1196W_fzgP22k5Lnadc53-Ghaeqg0cTjabKBrHFCmgg9cKfFmFgYtSdBohSCzozeoKolnHrCfZGBjjU2-vs_orDeyDvzElHli7Cyw7f5mjStB2te7fyD928wia4srZ9MAHJdKXlttJeKgBbn5LlSWJFdu88dX8U2qV4Jo96NtXdw";
//// Use a test public key:
//	private String jwt = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiT2xpdmllciBEcmllc2JhY2giLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1CSVJmT3J5dThVYy9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBQS9BQ0hpM3JlSUVVd21VSldqbTNoWmlFRXZCNjlJb2N1OHN3L3M5Ni1jL3Bob3RvLmpwZyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9jdXJhbnRpLW12cCIsImF1ZCI6ImN1cmFudGktbXZwIiwiYXV0aF90aW1lIjoxNTYyMTUyMzU2LCJ1c2VyX2lkIjoibjVNSEcyVEx1cVliemM4QXJoNkx3VkxmaVp1MiIsInN1YiI6Im41TUhHMlRMdXFZYnpjOEFyaDZMd1ZMZmladTIiLCJpYXQiOjE1NjIxNTIzNTYsImVtYWlsIjoib2xpdmllci5kcmllc2JhY2hAY3VyYW50aS5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJnb29nbGUuY29tIjpbIjExNTI3NDUyODg1NjQ2NDY0NDA1MiJdLCJlbWFpbCI6WyJvbGl2aWVyLmRyaWVzYmFjaEBjdXJhbnRpLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6Imdvb2dsZS5jb20ifX0.OLgwhtHzjhVmJV9U_lVCWkyPlRem5tyK8BIdRw2y7nDQzAkoh9sj2O_7uND7D6qSSgW9FQyEpXSdHfXE-YVYO6yHR_IJAt7ATawVtl4krVuKevDGOC6aVHncbfvYENvs-ADCWKYQ6-teEwC99S7_1iwcD5okuH4BQ2aOQe4XW7-7qVdhDHw0Q3BzPe9wlEoVqS1vebmxXBW-Rgt4Z-1LcMzYKVm5YM2Uiw-vibxAFRD1QbM1pm-3uQYlFrpHy5ksiuh4YoaIeZKiJKZNOEOzJ-XmdtvYwIWmtjZ5gI2iol75kQNJ00A2aF7H2pN90v6DTTxvaAT67-Xoc8stYx1XUA";
//
//	@BeforeAll
//	@DisplayName("Deploy a verticle")
//	static void prepare(Vertx vertx, VertxTestContext testContext) {
//		vertx.deployVerticle(ServerSmartDevice.class.getName(), new DeploymentOptions(), testContext.succeedingThenComplete());
//		try {
//			Thread.sleep(1000); // Need else in debug I have race condition error with Netty 4.1.87 with SSL = in verticle I have a sslwebclient and in test too = in test the webclient closing make impossible ssl connection....
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//	}
//
//	@AfterEach
//	@DisplayName("Check that the verticle is still there")
//	void lastChecks(Vertx vertx) {
//		assertThat(vertx.deploymentIDs()).isNotEmpty().hasSize(1);
//	}
//
//	@RepeatedTest(3)
//	@DisplayName("Test connection")
//	@Order(1)
//	void testConnection(Vertx vertx, VertxTestContext testContext) throws Throwable {
//		HttpClient client = vertx.createHttpClient();
//		client.request(HttpMethod.GET, 8080, "localhost", "/health")
//				.compose(req -> req.send().compose(HttpClientResponse::body))
//				.onComplete(testContext.succeeding(buffer -> testContext.verify(() -> {
//					assertThat(new JsonObject(buffer).getString("status")).isEqualTo("UP");
//					testContext.completeNow();
//				})));
//	}
//
//	@Test
//	@DisplayName("Test Creation SmartDevice")
//	@Order(10)
//	public void testCreateSmartDevice(Vertx vertx, VertxTestContext testContext) throws IOException {
//
//		Checkpoint smartDeviceCreated = testContext.checkpoint(2);
//
//		WebClient client = WebClient.create(vertx);
//		final String uid = UUID.randomUUID().toString();
//		// Create SmartDevice
//		JsonObject Register1 = new JsonObject().put(ATTRIBUTE_KEY, "key1234").put(ATTRIBUTE_TYPE, "type1")
//				.put(ATTRIBUTE_DESCRIPTION, "desc");
//		JsonObject Register2 = new JsonObject().put(ATTRIBUTE_KEY, "key4321").put(ATTRIBUTE_TYPE, "type2")
//				.put(ATTRIBUTE_DESCRIPTION, "desc2");
//		JsonArray measuresList = new JsonArray().add(Register1).add(Register2);
//		client.post(port, host, "/devices").putHeader(HEADER_ID_PARTNER, "1").bearerTokenAuthentication(jwt)
//				.sendJsonObject(
//						new JsonObject().put(ATTRIBUTE_SMART_DEVICE_ID, uid)
//								.put(ATTRIBUTE_URI, "scs://12345@192.168.1.35:20000").put(ATTRIBUTE_TYPE, "TEST")
//								.put(ATTRIBUTE_MEASURES, measuresList).put(ATTRIBUTE_ACTIVE, true),
//						testContext.succeeding(ar -> testContext.verify(() -> {
//							assertEquals(201, ar.statusCode());
//							smartDeviceCreated.flag();
//						})));
//
//		final String uid2 = UUID.randomUUID().toString();
//		client.post(port, host, "/devices").putHeader(HEADER_ID_PARTNER, "1").bearerTokenAuthentication(jwt)
//				.sendJsonObject(
//						new JsonObject().put(ATTRIBUTE_SMART_DEVICE_ID, uid2)
//								.put(ATTRIBUTE_URI, "scs://12345@192.168.1.35:20000").put(ATTRIBUTE_TYPE, "TEST")
//								.put(ATTRIBUTE_MEASURES, measuresList).put(ATTRIBUTE_ACTIVE, true),
//						testContext.succeeding(ar -> testContext.verify(() -> {
//							assertEquals(201, ar.statusCode());
//							smartDeviceCreated.flag();
//						})));
//
//	}
//
//	@Test
//	@DisplayName("Test Read SmartDevice")
//	@Order(20)
//	public void testReadSmartDevice(Vertx vertx, VertxTestContext testContext) throws IOException {
//
//		Checkpoint smartDeviceCreated = testContext.checkpoint(3);
//		WebClient client = WebClient.create(vertx);
//
//		// Get all Smartdevices
//		client.get(port, host, "/devices").bearerTokenAuthentication(jwt)
//				.send(testContext.succeeding(ar -> testContext.verify(() -> {
//					assertEquals(200, ar.statusCode());
//					JsonArray body = ar.bodyAsJsonArray();
//					assertEquals(2, body.size());
//					String uid = body.getJsonObject(1).getString("id");
//
//					// Get SmartDevice with uid
//					client.get(port, host, "/devices/" + uid).bearerTokenAuthentication(jwt)
//							.send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//								assertEquals(200, ar2.statusCode());
//								JsonObject device = ar2.bodyAsJsonObject();
//								assertEquals(uid, device.getString(ATTRIBUTE_SMART_DEVICE_ID));
//								assertEquals("1", device.getString(ATTRIBUTE_ID_PARTNER));
//								assertEquals("scs://12345@192.168.1.35:20000", device.getString(ATTRIBUTE_URI));
//								assertEquals("TEST", device.getString(ATTRIBUTE_TYPE));
//								assertEquals(2, device.getJsonArray(ATTRIBUTE_MEASURES).size());
//								assertEquals("true", device.getString(ATTRIBUTE_ACTIVE));
//								assertEquals("false", device.getString(ATTRIBUTE_DELETED));
//								smartDeviceCreated.flag();
//							})));
//
//				})));
//
//		// Get only some SmartDevices //TODO Test with character '&' in uri...
//		client.get(port, host, "/devices?uri=scs://12345@192.168.1.35:20000").bearerTokenAuthentication(jwt)
//				.send(testContext.succeeding(ar -> testContext.verify(() -> {
//					assertEquals(200, ar.statusCode());
//					assertEquals(1, ar.bodyAsJsonArray().size());
//					JsonObject device = ar.bodyAsJsonArray().getJsonObject(0);
//					assertNotNull(device.getString(ATTRIBUTE_SMART_DEVICE_ID));
//					assertEquals("1", device.getString(ATTRIBUTE_ID_PARTNER));
//					assertEquals("scs://12345@192.168.1.35:20000", device.getString(ATTRIBUTE_URI));
//					assertEquals("TEST", device.getString(ATTRIBUTE_TYPE));
//					assertEquals(2, device.getJsonArray(ATTRIBUTE_MEASURES).size());
//					assertEquals("true", device.getString(ATTRIBUTE_ACTIVE));
//					assertEquals("false", device.getString(ATTRIBUTE_DELETED));
//
//					smartDeviceCreated.flag();
//				})));
//
//		// Get no SmartDevices with unknown uri
//		client.get(port, host, "/devices?uri=scs://unknowuri").bearerTokenAuthentication(jwt)
//				.send(testContext.succeeding(ar -> testContext.verify(() -> {
//					assertEquals(200, ar.statusCode());
//					assertEquals(0, ar.bodyAsJsonArray().size());
//					smartDeviceCreated.flag();
//				})));
//
//	}
//
//	@Test
//	@DisplayName("Test Update SmartDevice")
//	@Order(40)
//	public void testUpdateSmartDevice(Vertx vertx, VertxTestContext testContext) throws IOException {
//		Checkpoint smartDeviceUpdated = testContext.checkpoint(2);
//		WebClient client = WebClient.create(vertx);
//
//		// Get all Smartdevices
//		client.get(port, host, "/devices").bearerTokenAuthentication(jwt)
//				.send(testContext.succeeding(ar -> testContext.verify(() -> {
//					assertEquals(200, ar.statusCode());
//					JsonArray body = ar.bodyAsJsonArray();
//					assertEquals(2, body.size());
//					String uid = body.getJsonObject(1).getString("id");
//
//					// Get SmartDevice with uid
//					client.put(port, host, "/devices/" + uid).bearerTokenAuthentication(jwt).sendJsonObject(
//							new JsonObject().put(ATTRIBUTE_SMART_DEVICE_ID, uid)
//									.put(ATTRIBUTE_URI, "scs://12345@192.168.1.35:20001").put(ATTRIBUTE_TYPE, "TEST2")
//									.put(ATTRIBUTE_ACTIVE, true),
//							testContext.succeeding(ar2 -> testContext.verify(() -> {
//								assertEquals(200, ar2.statusCode());
//								JsonObject device = ar2.bodyAsJsonObject();
//								assertEquals(uid, device.getString(ATTRIBUTE_SMART_DEVICE_ID));
//								assertEquals("1", device.getString(ATTRIBUTE_ID_PARTNER));
//								assertEquals("scs://12345@192.168.1.35:20001", device.getString(ATTRIBUTE_URI));
//								assertEquals("TEST2", device.getString(ATTRIBUTE_TYPE));
//								assertEquals(2, device.getJsonArray(ATTRIBUTE_MEASURES).size());
//								assertEquals("true", device.getString(ATTRIBUTE_ACTIVE));
//								assertEquals("false", device.getString(ATTRIBUTE_DELETED));
//
//								smartDeviceUpdated.flag();
//							})));
//
//					// Test to retrieve with uid
//					client.get(port, host, "/devices/" + uid).bearerTokenAuthentication(jwt)
//							.send(testContext.succeeding(ar3 -> testContext.verify(() -> {
//								JsonObject device = ar3.bodyAsJsonObject();
//								assertEquals(uid, device.getString(ATTRIBUTE_SMART_DEVICE_ID));
//								assertEquals("1", device.getString(ATTRIBUTE_ID_PARTNER));
//								assertEquals("scs://12345@192.168.1.35:20001", device.getString(ATTRIBUTE_URI));
//								assertEquals("TEST2", device.getString(ATTRIBUTE_TYPE));
//								assertEquals(2, device.getJsonArray(ATTRIBUTE_MEASURES).size());
//								assertEquals("true", device.getString(ATTRIBUTE_ACTIVE));
//								assertEquals("false", device.getString(ATTRIBUTE_DELETED));
//								smartDeviceUpdated.flag();
//							})));
//
//				})));
//	}
//
//	@Test
//	@DisplayName("Test Delete SmartDevice")
//	@Order(50)
//	public void testDeleteSmartDevice(Vertx vertx, VertxTestContext testContext) throws IOException {
//
//		Checkpoint smartDeviceDeleted = testContext.checkpoint(2);
//		WebClient client = WebClient.create(vertx);
//
//		// Get all Smartdevices
//		client.get(port, host, "/devices").bearerTokenAuthentication(jwt)
//				.send(testContext.succeeding(ar -> testContext.verify(() -> {
//					assertEquals(200, ar.statusCode());
//					JsonArray body = ar.bodyAsJsonArray();
//					assertEquals(2, body.size());
//					String uid = body.getJsonObject(1).getString("id");
//
//					// Delete SmartDevice
//					client.delete(port, host, "/devices/" + uid).bearerTokenAuthentication(jwt)
//							.send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//								assertEquals(204, ar2.statusCode());
//								assertEquals(null, ar2.bodyAsJsonObject());
//
//								// Test to retrieve with uid
//								client.get(port, host, "/devices/" + uid).bearerTokenAuthentication(jwt)
//										.send(testContext.succeeding(ar3 -> testContext.verify(() -> {
//											assertEquals(200, ar3.statusCode());
//											assertEquals("true", ar3.bodyAsJsonObject().getString(ATTRIBUTE_DELETED));
//											smartDeviceDeleted.flag();
//										})));
//
//								// Test to retrieve all smartdevice
//								client.get(port, host, "/devices").bearerTokenAuthentication(jwt)
//										.send(testContext.succeeding(ar4 -> testContext.verify(() -> {
//											assertEquals(200, ar4.statusCode());
//											JsonArray body2 = ar4.bodyAsJsonArray();
//											assertEquals(1, body2.size());
//											smartDeviceDeleted.flag();
//										})));
//
//							})));
//
//				})));
//	}
//}

package com.domosnap.smartdevice.infra;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;


@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SmartDeviceMeasureQuarkusTestAPITest {

    private static final String ATTRIBUTE_SMART_DEVICE_ID = "id";
    private static final String ATTRIBUTE_URI = "uri";
    private static final String ATTRIBUTE_TYPE = "type";
    private static final String ATTRIBUTE_MEASURES = "measures";
    private static final String ATTRIBUTE_ACTIVE = "active";

    private static final String ATTRIBUTE_DESCRIPTION = "description";

    private static final String ATTRIBUTE_KEY = "key";

    // Use a test public key:
    private final String jwt = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik9saXZpZXIgRHJpZXNiYWNoIiwiaWF0IjoxNTE2MjM5MDIyLCJleHAiOjIwMTYyMzkwMjIsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9yZWFsbXMvbWFzdGVyIiwiYXVkIjoic21hcnRkZXZpY2UtcmVzdCIsInVzZXJfaWQiOiJuNU1IRzJUTHVxWWJ6YzhBcmg2THdWTGZpWnUyIiwiZW1haWwiOiJvbGl2aWVyLmRyaWVzYmFjaEBjdXJhbnRpLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlfQ.DzUZ7R0Xk2A6nAwa3rM5WOk804YNd8Tjvo3yBYjBQUxlMb8vusTlMD07pJjahg3zMXgtofogvg_F2TiAl67dKUVyxUeebUvTBvxZlg6tI5OSS-0mKvs4wX1fSk70H6hf9Axl3MuX0DCR--ln7VRGYTNqPsFY5MkPlWQ8Amv2RVdxxv6FIlEGrPmaXdSwJcB183H67hGIMktLVsPckx8lNuILwHKCaqZ3RPEf1YeqYivlkAWkkfRA-YQQPD8sX1-tRbWeL1AQe41NDe4r5xvTGwRc0Waetva1NHWReWxeYACBe7uH9_lgwjDXWrix0qWrUOGB3DfjulBtZUpRiQVI-Q";

    static String uid = UUID.randomUUID().toString();
    static String uid2 = UUID.randomUUID().toString();


    @Test
    @DisplayName("Test Creation SmartDevice")
    @Order(40)
    public void testCreateSmartDevice() {


        JsonObject Register1 = new JsonObject().put(ATTRIBUTE_KEY, "key1234").put(ATTRIBUTE_TYPE, "type1")
                .put(ATTRIBUTE_DESCRIPTION, "desc");
        JsonObject Register2 = new JsonObject().put(ATTRIBUTE_KEY, "key4321").put(ATTRIBUTE_TYPE, "type2")
                .put(ATTRIBUTE_DESCRIPTION, "desc2");
        JsonArray measuresList = new JsonArray().add(Register1).add(Register2);

        given()
                .auth().oauth2(jwt)
                .contentType(ContentType.JSON)
                .body(new JsonObject()
                        .put(ATTRIBUTE_SMART_DEVICE_ID, uid)
                        .put(ATTRIBUTE_URI, "scs://12345@192.168.1.35:20000")
                        .put(ATTRIBUTE_TYPE, "TEST")
                        .put(ATTRIBUTE_MEASURES, measuresList)
                        .put(ATTRIBUTE_ACTIVE, true).toString())
                .when().post("/devices")
                .then()
                .statusCode(201)
                .and()
                .body(ATTRIBUTE_SMART_DEVICE_ID, is(uid));


        given()
                .auth().oauth2(jwt)
                .contentType(ContentType.JSON)
                .body(new JsonObject()
                        .put(ATTRIBUTE_SMART_DEVICE_ID, uid2)
                        .put(ATTRIBUTE_URI, "scs://12345@192.168.1.35:20000")
                        .put(ATTRIBUTE_TYPE, "TEST")
                        .put(ATTRIBUTE_MEASURES, measuresList)
                        .put(ATTRIBUTE_ACTIVE, true).toString())
                .when().post("/devices")
                .then()
                .statusCode(201)
                .and()
                .body(ATTRIBUTE_SMART_DEVICE_ID, is(uid2));

    }

    @Test
    @DisplayName("Test Creation Measure")
    @Order(50)
    public void testCreateMeasure() {

        String key = "1234key";

        given()
                .auth().oauth2(jwt)
                .contentType(ContentType.JSON)
                .body(new JsonObject()
                        .put(ATTRIBUTE_KEY, key)
                        .put(ATTRIBUTE_TYPE, "type3")
                        .put(ATTRIBUTE_DESCRIPTION, "desc3").toString())
                .when().post("/devices/" + uid + "/measures")
                .then()
                .statusCode(201)
                .and()
                .body(ATTRIBUTE_KEY, is(key))
                .body(ATTRIBUTE_TYPE, is("type3"))
                .body(ATTRIBUTE_DESCRIPTION, is("desc3"));

        // get measure
        given()
                .auth().oauth2(jwt)
                .when().get("/devices/" + uid + "/measures/" + key)
                .then()
                .statusCode(200)
                .and()
                .body(ATTRIBUTE_KEY, is(key))
                .body(ATTRIBUTE_TYPE, is("type3"))
                .body(ATTRIBUTE_DESCRIPTION, is("desc3"));
    }

    @Test
    @DisplayName("Test Read Measure")
    @Order(60)
    public void testReadMeasure(){

        String key = "1234key";

        given()
                .auth().oauth2(jwt)
                .when().get("/devices/" + uid + "/measures/" + key)
                .then()
                .statusCode(200);

    }

    @Test
    @DisplayName("Test Update Measure")
    @Order(70)
    public void testUpdateMeasure(){

        String key = "1234key";

        given()
                .auth().oauth2(jwt)
                .when().get("/devices/" + uid + "/measures/" + key)
                .then()
                .statusCode(200)
                .and()
                .body(ATTRIBUTE_KEY, is(key))
                .body(ATTRIBUTE_TYPE, is("type3"))
                .body(ATTRIBUTE_DESCRIPTION, is("desc3"));

        // Update Measure
        given()
                .auth().oauth2(jwt)
                .contentType(ContentType.JSON)
                .body("{\"key\": \"" + key + "\", \"type\": \"type3bis\", \"description\": \"desc3bis\"}")
                .when().put("/devices/" + uid + "/measures/" + key)
                .then()
                .statusCode(200)
                .and()
                .body(ATTRIBUTE_KEY, is(key))
                .body(ATTRIBUTE_TYPE, is("type3bis"))
                .body(ATTRIBUTE_DESCRIPTION, is("desc3bis"));


        given()
                .auth().oauth2(jwt)
                .when().get("/devices/" + uid + "/measures/" + key)
                .then()
                .statusCode(200)
                .and()
                .body(ATTRIBUTE_KEY, is(key))
                .body(ATTRIBUTE_TYPE, is("type3bis"))
                .body(ATTRIBUTE_DESCRIPTION, is("desc3bis"));
    }

    @Test
    @DisplayName("Test Delete Measure")
    @Order(80)
    public void testDeleteMeasure(){

        String key = "1234key";

        given()
                .auth().oauth2(jwt)
                .when().get("/devices/" + uid + "/measures/" + key)
                .then()
                .statusCode(200)
                .and()
                .body(ATTRIBUTE_KEY, is(key))
                .body(ATTRIBUTE_TYPE, is("type3bis"))
                .body(ATTRIBUTE_DESCRIPTION, is("desc3bis"));

        // delete Measure
        given()
                .auth().oauth2(jwt)
                .contentType(ContentType.JSON)
                .when().delete("/devices/" + uid + "/measures/" + key)
                .then()
                .statusCode(204);
    }

}

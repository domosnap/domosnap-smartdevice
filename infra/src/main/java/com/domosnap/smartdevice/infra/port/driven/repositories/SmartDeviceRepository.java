/* SmartDeviceRepository
 * this is the repository of the smart device
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.smartdevice.infra.port.driven.repositories;


import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;


import com.domosnap.smartdevice.domain.metamodel.Extension;
import com.domosnap.smartdevice.domain.smartdevice.SmartDevice;
import com.domosnap.smartdevice.domain.smartdevice.SmartDeviceId;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStore;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
import com.domosnap.tools.core.services.cqrs.infra.repository.AsyncAggregateRepository;

public class SmartDeviceRepository extends AsyncAggregateRepository<SmartDevice> {

	private static EventStore eventS;
	
    public SmartDeviceRepository(EventStore eventStore) {
        super(eventStore);
    }

    @Override
    protected SmartDevice fromHistory(List<Event<?>> history) {
        return new SmartDevice(history);
    }


    /**
     * Crée un SmartDevice de manière asynchrone
     *
     * @param smartDeviceId
     * @param uri
     * @param type
     * @param active
     * @param virtual
     * @param extension
     * @param principal
     * @param eventPublisher
     * @return
     */
    public CompletableFuture<SmartDevice> create(SmartDeviceId smartDeviceId, String uri, String type,
                                                 Boolean active, Boolean virtual, Extension extension, ResourceName principal, Consumer<Event<?>> eventPublisher) {

        // Exécution asynchrone de la méthode statique create
        CompletableFuture<Void> creation = CompletableFuture.runAsync(() -> {
            SmartDevice.create(smartDeviceId, uri, type, active, virtual, extension, principal, eventPublisher);
        });

        return creation.thenCompose(v -> this.getById(smartDeviceId.getId()));
    }



    public void close() {
        eventS.close();
    }
}

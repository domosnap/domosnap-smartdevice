/* UpdateDirectoryHandler
 * this will handle the link between the smart devices and the directory
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.smartdevice.infra.handlers;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import com.domosnap.smartdevice.domain.directory.Directory;
import com.domosnap.smartdevice.domain.smartdevice.events.SmartDeviceCreatedEvent;
import com.domosnap.smartdevice.domain.smartdevice.events.SmartDeviceDeleteEvent;
import com.domosnap.smartdevice.infra.port.driven.repositories.DirectoryRepository;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.domain.EventPublisher;

public class UpdateDirectoryEventHandler implements EventPublisher {

	private Map<Class<?>, Consumer<Event<?>>> appliers = new HashMap<>();
	
	private Consumer<Event<?>> dispatchEvent;

	@SuppressWarnings("unchecked")
	public <T extends Event<?>> void register(Class<T> eventClass, Consumer<T> eventConsumer) {
		appliers.put(eventClass, (Consumer<Event<?>>) eventConsumer);
	}

	private DirectoryRepository directoryRepository;
/**
 * this will create a update directoryhandler 
 * @param directoryRepository directory repository
 * @param dispatchEvent consumer
 */
	public UpdateDirectoryEventHandler(DirectoryRepository directoryRepository, Consumer<Event<?>> dispatchEvent) {
		this.directoryRepository = directoryRepository;
		this.dispatchEvent = dispatchEvent;
		register(SmartDeviceCreatedEvent.class, this::apply);
		register(SmartDeviceDeleteEvent.class, this::apply);
	}


	@Override
	public void accept(Event<?> event) {
		Consumer<Event<?>> consumer = appliers.get(event.getClass());
		if (consumer != null) {
			consumer.accept(event);
		}
	}

	public void apply(SmartDeviceCreatedEvent event) {
		directoryRepository.getById(Directory.mainDirectory.toString())
				.thenAccept(directory -> {
					directory.addSmartDevice(event.getAggregateId(), event.getUri(), event.getCreator(), dispatchEvent);
				});
	}

	public void apply(SmartDeviceDeleteEvent event) {
		directoryRepository.getById(Directory.mainDirectory.toString())
				.thenAccept(directory -> {
					directory.removeSmartDevice(event.getAggregateId(), event.getCreator(), dispatchEvent);
				});
	}
}

/* DirectoryRepository
 * this is the repository of the directory
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.smartdevice.infra.port.driven.repositories;



import java.util.List;
import java.util.concurrent.CompletableFuture;


import com.domosnap.smartdevice.domain.directory.Directory;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStore;
import com.domosnap.tools.core.services.cqrs.infra.repository.AsyncAggregateRepository;



public class DirectoryRepository extends AsyncAggregateRepository<Directory> {

    public DirectoryRepository(EventStore eventStore) {
        super(eventStore);
    }

    @Override
    protected Directory fromHistory(List<Event<?>> history) {
        return new Directory(history);
    }


    public CompletableFuture<Directory> getDirectory() {
        return getById(Directory.mainDirectory.getId());
    }

    
    public void close() {
    	super.eventStore.close();
    }
}

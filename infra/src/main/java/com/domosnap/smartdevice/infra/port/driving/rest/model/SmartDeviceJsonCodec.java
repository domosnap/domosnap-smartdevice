/* SmartDeviceJsonCodec
 * Transform a smart device into a register
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.smartdevice.infra.port.driving.rest.model;

import java.util.List;

import com.domosnap.smartdevice.domain.smartdevice.SmartDevice;

public class SmartDeviceJsonCodec {
	private SmartDeviceJsonCodec()
	{
		
	}
	/**
	 * take a list of smart device and transform it into JSON
	 * @param smartDevices the list of smart devices
	 * @return JSON of the list of smart devices
	 */
	public static final String DevicestoJson(List<SmartDevice> smartDevices) {
		if (smartDevices == null) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (SmartDevice smartDevice : smartDevices) 
		{ 
			sb.append(SmartDeviceJsonCodec.toJson(smartDevice));
			sb.append(",");
		}
		if (sb.length() > 1) {
			sb.setLength(sb.length()-1);
		}
		sb.append("]");
	    return sb.toString();
	}
	/**
	 * take a smart device and transform it into JSON
	 * @param smartDevice the smart device
	 * @return the JSON of the smart device
	 */
	public static final String toJson(SmartDevice smartDevice) {
		if (smartDevice == null) {
			return "";
		}
	    StringBuilder sb = new StringBuilder();
	    sb.append("{\"id\":\"").append(smartDevice.getId()).append("\",")
	      .append("\"uri\":\"").append(smartDevice.getUri()).append("\",")
	      .append("\"type\":\"").append(smartDevice.getType()).append("\",")
	      .append("\"active\":\"").append(smartDevice.isActive()).append("\",")
	      .append("\"virtual\":\"").append(smartDevice.isVirtual()).append("\",")
	      .append("\"measures\":").append(RegisterJsonCodec.registerListToJson(smartDevice)).append(",")
	      .append("\"deleted\":").append(smartDevice.getDeleted()).append("}");
	
	    return sb.toString();
	}
}

package com.domosnap.smartdevice.infra.launcher;

import com.domosnap.smartdevice.infra.handlers.UpdateDirectoryEventHandler;
import com.domosnap.smartdevice.infra.port.driven.repositories.*;
import com.domosnap.tools.core.services.cqrs.domain.EventPublisher;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.impl.InMemoryEventStore;
import com.domosnap.tools.core.services.cqrs.infra.handler.*;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStore;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStoreFactory;
import com.domosnap.smartdevice.domain.directory.Directory;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
import com.domosnap.tools.core.services.eventstore.KafkaEventStore;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@ApplicationScoped
public class InfrastructureInitializer {

    private static final Logger log = Logger.getLogger(InfrastructureInitializer.class.getName());
    private final String kafkaServers;
    private final String kafkaTopicSmartdevice;
    private final String kafkaTopicSmartdeviceDirectory;

    private SmartDeviceRepository smartDeviceRepository;
    private DirectoryRepository directoryRepository;
    private EventPublisher eventPublisher;

    public InfrastructureInitializer(
            @ConfigProperty(name = "kafka.bootstrap.servers") String kafkaServers,
            @ConfigProperty(name = "kafka.topic.main") String kafkaTopic,
            @ConfigProperty(name = "kafka.topic.repo") String kafkaTopicRepo,
            @ConfigProperty(name = "event.store.class") String eventStoreClass
    ) {
        this.kafkaServers = kafkaServers;
        this.kafkaTopicSmartdevice = kafkaTopic;
        this.kafkaTopicSmartdeviceDirectory = kafkaTopicRepo;

        DispatchEventPublisher smartDeviceEventDispatcher = new DispatchEventPublisher();
        this.smartDeviceRepository = createSmartDeviceRepository(eventStoreClass, smartDeviceEventDispatcher);
        DispatchEventPublisher directoryEventDispatcher = new DispatchEventPublisher();
        this.directoryRepository = createDirectoryRepository(eventStoreClass, directoryEventDispatcher);
        UpdateDirectoryEventHandler udh = new UpdateDirectoryEventHandler(directoryRepository, directoryEventDispatcher);
        smartDeviceEventDispatcher.register(udh);
        eventPublisher = smartDeviceEventDispatcher;

    }

    private EventStore createEventStore(String eventStoreClass, String groupId, String topic) {
        Map<String, Object> config = new HashMap<>();

        if (eventStoreClass.equals("com.domosnap.tools.core.services.eventstore.KafkaEventStore")) {
            config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServers);
            config.put(KafkaEventStore.TOPIC_CONFIG, topic);
            config.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        }

        EventStore store;
        if (eventStoreClass.equals("com.domosnap.tools.core.services.cqrs.infra.eventStore.impl.InMemoryEventStore")) {
            store = new InMemoryEventStore();
        } else if (eventStoreClass.equals("com.domosnap.tools.core.services.eventstore.KafkaEventStore")) {
            store = new KafkaEventStore();
        } else {
            throw new IllegalArgumentException("EventStore type not supported: " + eventStoreClass);
        }
        store.init(config);
        return store;
    }

    private SmartDeviceRepository createSmartDeviceRepository(String eventStoreClass, DispatchEventPublisher eventPublisher) {
        EventStore eventS = createEventStore(eventStoreClass, "smartdevice-group", this.kafkaTopicSmartdevice);

        SmartDeviceRepository sr = new SmartDeviceRepository(eventS);
        eventPublisher.register(new PersistingEventPublisher(eventS));

        return sr;
    }

    private DirectoryRepository createDirectoryRepository(String eventStoreClass, DispatchEventPublisher eventPublisher) {
        EventStore eventStore = createEventStore(eventStoreClass, "directory-group-smartdevice", this.kafkaTopicSmartdeviceDirectory);

        DirectoryRepository dr = new DirectoryRepository(eventStore);
        eventPublisher.register(new PersistingEventPublisher(eventStore));

        try {
            dr.getById(Directory.mainDirectory.getId()).get();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            Directory.create(new ResourceName("Profile", "directory", "system"), eventPublisher);
            log.info("Directory créé.");
        }
        return dr;
    }

    public SmartDeviceRepository getSmartDeviceRepository() {
        return smartDeviceRepository;
    }

    public DirectoryRepository getDirectoryRepository() {
        return directoryRepository;
    }

    public EventPublisher getEventPublisher() {
        return eventPublisher;
    }

}
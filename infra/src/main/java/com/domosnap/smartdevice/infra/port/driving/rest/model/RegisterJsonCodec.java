/* RegisterJsonCodec
 * Transform a register into JSON
 * Raphaël Lopes
 * 06.06.2020
 */

package com.domosnap.smartdevice.infra.port.driving.rest.model;

import java.util.List;

import com.domosnap.smartdevice.domain.smartdevice.Measure;
import com.domosnap.smartdevice.domain.smartdevice.SmartDevice;

public class RegisterJsonCodec {
	
	private RegisterJsonCodec() {
	}
	/**
	 * take a register and return every register in a JSON
	 * @param smartDevice the smartDevice
	 * @return JSON contract list
	 */
	public static final String registerListToJson(SmartDevice smartDevice) {
		return getList(smartDevice.getMeasureList());
	}
	/**
	 * take a register and transform it to a JSON
	 * @param smartDevice the smart device
	 * @param key of the register
	 * @return JSON Register
	 */
	public static final String toJson(SmartDevice smartDevice,String key) {
		StringBuilder sb = new StringBuilder();
		Measure register = smartDevice.getMeasure(key);
		sb.append("{\"key\":\"").append(register.getKey()).append("\",")
	      .append("\"type\":\"").append(register.getType()).append("\",")
	      .append("\"description\":\"").append(register.getDescription()).append("\"}");
		return sb.toString();
	}
	/**
	 * Get a list of register and transform into JSON
	 * @param registerList the list of register
	 * @return JSON of the register list
	 */
	public static final String getList(List<Measure> registerList) {
		StringBuilder sb = new StringBuilder("[");
		for (Measure register : registerList) {
			sb.append("{\"key\":\"").append(register.getKey()).append("\",")
		      .append("\"type\":\"").append(register.getType()).append("\",")
		      .append("\"description\":\"").append(register.getDescription()).append("\"},");
		}
		if (sb.length() > 1) {
			sb.setLength(sb.length()-1);
		}
		
		sb.append("]");
		return sb.toString();
	}	
}

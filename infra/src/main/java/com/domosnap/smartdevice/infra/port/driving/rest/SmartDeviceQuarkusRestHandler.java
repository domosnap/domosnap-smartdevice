package com.domosnap.smartdevice.infra.port.driving.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import com.domosnap.smartdevice.domain.metamodel.smartgrid.EmptyExtension;
import com.domosnap.smartdevice.domain.smartdevice.Measure;
import com.domosnap.smartdevice.domain.smartdevice.SmartDevice;
import com.domosnap.smartdevice.domain.smartdevice.SmartDeviceId;
import com.domosnap.smartdevice.infra.handlers.UpdateDirectoryEventHandler;
import com.domosnap.smartdevice.infra.port.driven.repositories.DirectoryRepository;
import com.domosnap.smartdevice.infra.port.driven.repositories.SmartDeviceRepository;
import com.domosnap.smartdevice.domain.directory.Directory;
import com.domosnap.smartdevice.infra.port.driving.rest.model.RegisterJsonCodec;
import com.domosnap.smartdevice.infra.port.driving.rest.model.SmartDeviceJsonCodec;
import com.domosnap.tools.core.services.cqrs.domain.EventPublisher;
import com.domosnap.tools.core.services.cqrs.infra.handler.DispatchEventPublisher;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
import io.quarkus.runtime.Quarkus;
import io.quarkus.security.Authenticated;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import jakarta.ws.rs.core.*;
import io.quarkus.vertx.web.Route;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import jakarta.inject.Inject;
import com.domosnap.smartdevice.infra.launcher.InfrastructureInitializer;


@Authenticated
public class SmartDeviceQuarkusRestHandler {
    private static final String SMART_DEVICE_ID = "id";
    private static final String SMART_DEVICE_URI = "uri";
    private static final String SMART_DEVICE_KEY = "key";
    private static final String SMART_DEVICE_DESCRIPTION = "description";
    private static final String SMART_DEVICE_TYPE = "type";
    private static final String SMART_DEVICE_MEASURES = "measures";
    private static final String SMART_DEVICE_ACTIVE = "active";
    private static final String SMART_DEVICE_VIRTUAL = "virtual";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String APP_CHARSET = "application/json;charset=UTF-8";
    private static final String SMART_DEVICE = "SmartDevice";
    private static final String SMARTDEVICEID = "smartDeviceId";
    private static  final String USER_ID_STRING = "user_id";

    @ConfigProperty(name = "modedegrade", defaultValue="false")
    Boolean modedegrade;

    private final Logger log = Logger.getLogger(SmartDeviceQuarkusRestHandler.class.getName());


    String externalApiUrl = "http://localhost:8085/devices";

    private final SmartDeviceRepository smartDeviceRepository;
    private final DirectoryRepository directoryRepository;
    private final EventPublisher eventPublisher;
    private final HttpClient httpClient;

    @Inject
    public SmartDeviceQuarkusRestHandler(InfrastructureInitializer infra) {
        this.smartDeviceRepository = infra.getSmartDeviceRepository();
        this.directoryRepository = infra.getDirectoryRepository();
        this.eventPublisher = infra.getEventPublisher();
        this.httpClient = HttpClient.newHttpClient();
    }

    /**
     * Create a smart device
     *
     * @param rc routing context
     */
    @Route(path = "/devices", methods = Route.HttpMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON, order = 2)
    public void createSmartDevice(RoutingContext rc) {
        String creatorId = rc.user().principal().getString(USER_ID_STRING);
        JsonObject body = rc.getBodyAsJson();
        String smartDeviceId = body.getString(SMART_DEVICE_ID);

        if (smartDeviceId == null) {
            smartDeviceId = UUID.randomUUID().toString();
        }

        String uri = body.getString(SMART_DEVICE_URI);
        String type = body.getString(SMART_DEVICE_TYPE);
        JsonArray measureArray = body.getJsonArray(SMART_DEVICE_MEASURES);
        Boolean active = body.getBoolean(SMART_DEVICE_ACTIVE);
        Boolean virtual = body.getBoolean(SMART_DEVICE_VIRTUAL);
        String description = "desc";
        String title = "title";

        final String typee = type;
        final String finalSmartDeviceId = smartDeviceId;

        smartDeviceRepository.ifPresentOrElse(
                smartDeviceId,
                smartDevice -> rc.response().setStatusCode(409).end(),
                () -> {
                    SmartDeviceId uid = new SmartDeviceId(finalSmartDeviceId);
                    ResourceName creator = new ResourceName(ResourceName.DOMAIN_SMARTDEVICE, SMART_DEVICE, creatorId);

                    smartDeviceRepository.create(uid, uri, typee, active, virtual, new EmptyExtension(), creator, eventPublisher)
                            .thenAccept(smartDevice -> {
                                // Ajout des mesures
                                if (measureArray != null) {
                                    measureArray.forEach(object -> {
                                        if (object instanceof JsonObject) {
                                            JsonObject measure = (JsonObject) object;
                                            try {
                                                smartDevice.createMeasure(
                                                        measure.getString(SMART_DEVICE_KEY),
                                                        measure.getString(SMART_DEVICE_TYPE),
                                                        measure.getString(SMART_DEVICE_DESCRIPTION),
                                                        creator,
                                                        eventPublisher
                                                );
                                            } catch (Exception ex) {
                                                log.warning("Failed to create measure: " + ex.getMessage());
                                            }
                                        }
                                    });
                                }

                                // Appel API externe
                                if (!modedegrade) {
                                    HttpRequest externalApiRequest = HttpRequest.newBuilder()
                                            .uri(URI.create(externalApiUrl))
                                            .timeout(Duration.ofMinutes(2))
                                            .header(CONTENT_TYPE, "application/json")
                                            .POST(HttpRequest.BodyPublishers.ofString(
                                                    "{\"who\": \"" + typee + "\", \"where\": \"" + uri +
                                                            "\", \"description\": \"" + description + "\", \"title\": \"" + title + "\"}")
                                            )
                                            .build();

                                    httpClient.sendAsync(externalApiRequest, HttpResponse.BodyHandlers.ofString())
                                            .thenApply(response -> {
                                                if (response.statusCode() >= 400) {
                                                    System.err.println("Erreur de l'API externe: " + response.statusCode() + " - " + response.body());
                                                } else {
                                                    System.out.println("Réponse de l'API externe : " + response.body());
                                                }
                                                return response;
                                            });
                                }
                                rc.response()
                                        .putHeader("Location", rc.normalizedPath() + "/" + finalSmartDeviceId)
                                        .putHeader(CONTENT_TYPE, "application/json; charset=UTF-8")
                                        .setStatusCode(201)
                                        .end(SmartDeviceJsonCodec.toJson(smartDevice));
                            })
                            .exceptionally(ex -> {
                                log.severe("Error creating smart device: " + ex.getMessage());
                                rc.response().setStatusCode(500).end();
                                return null;
                            });
                }
        );
    }

    /**
     * Update a smart device
     * @param rc routing context
     */
    @Route(path = "/devices/:smartDeviceId", methods = Route.HttpMethod.PUT, consumes = MediaType.APPLICATION_JSON, order = 4)
    public void updateSmartDevice(RoutingContext rc) {
        String creatorId = rc.user().principal().getString(USER_ID_STRING);

        ResourceName creator = new ResourceName(ResourceName.DOMAIN_SMARTDEVICE, SMART_DEVICE, creatorId);
        JsonObject body = rc.getBodyAsJson();
        String smartDeviceId = body.getString(SMART_DEVICE_ID);

        if (smartDeviceId == null) {
            rc.response().setStatusCode(400).end("Need a smartdevice with an Id.");
            return;
        }

        String uri = body.getString(SMART_DEVICE_URI);
        String type = body.getString(SMART_DEVICE_TYPE);
        JsonArray measureArray = body.getJsonArray(SMART_DEVICE_MEASURES);
        Boolean active = body.getBoolean(SMART_DEVICE_ACTIVE);

        smartDeviceRepository.ifPresentOrElse(
                smartDeviceId,
                smartDevice -> {
                    List<Measure> noUpdatingMeasures = new ArrayList<>(smartDevice.getMeasureList());

                    // Gestion des mesures
                    if (measureArray != null) {
                        boolean hasInvalidMeasure = false;
                        String invalidMeasureMessage = "";

                        for (int i = 0; i < measureArray.size(); i++) {
                            Object object = measureArray.getValue(i);
                            if (object instanceof JsonObject) {
                                JsonObject measure = (JsonObject) object;
                                Measure m = smartDevice.getMeasure(measure.getString(SMART_DEVICE_KEY));

                                try {
                                    // Ajout
                                    if (m == null) {
                                        smartDevice.createMeasure(
                                                measure.getString(SMART_DEVICE_KEY),
                                                measure.getString(SMART_DEVICE_TYPE),
                                                measure.getString(SMART_DEVICE_DESCRIPTION),
                                                creator,
                                                eventPublisher
                                        );
                                    }
                                    // Modification
                                    else {
                                        smartDevice.updateMeasure(
                                                measure.getString(SMART_DEVICE_KEY),
                                                measure.getString(SMART_DEVICE_TYPE),
                                                measure.getString(SMART_DEVICE_DESCRIPTION),
                                                creator,
                                                eventPublisher
                                        );
                                        // Suppression de la liste pour ne pas la supprimer ensuite
                                        noUpdatingMeasures.remove(m);
                                    }
                                } catch (Exception e) {
                                    log.warning("Error updating/creating measure: " + e.getMessage());
                                }
                            } else {
                                hasInvalidMeasure = true;
                                invalidMeasureMessage = "Wrong measure: " + object.toString();
                            }
                        }

                        if (hasInvalidMeasure) {
                            rc.response().setStatusCode(400).end(invalidMeasureMessage);
                            return;
                        }

                        // Suppression des measures qui ne sont pas présentes dans la nouvelle liste
                        for (Measure measure : noUpdatingMeasures) {
                            try {
                                smartDevice.deleteMeasure(measure.getKey(), creator, eventPublisher);
                            } catch (Exception e) {
                                log.warning("Error deleting measure: " + e.getMessage());
                            }
                        }
                    }

                    try {
                        // Mise à jour du smart device
                        smartDevice.updateSmartDevice(uri, type, active, creator, eventPublisher);

                        // Réponse
                        rc.response()
                                .putHeader(CONTENT_TYPE, "application/json; charset=UTF-8")
                                .setStatusCode(200)
                                .end(SmartDeviceJsonCodec.toJson(smartDevice));
                    } catch (Exception e) {
                        log.severe("Error updating smart device: " + e.getMessage());
                        rc.response().setStatusCode(500).end();
                    }
                },
                () -> {
                    log.finest("Smart device not found: " + smartDeviceId);
                    rc.response().setStatusCode(404).end();
                }
        );
    }


    /**
     * Get all devices
     * @param rc routing context
     */
    @Route(path = "/devices", methods = Route.HttpMethod.GET, produces = MediaType.APPLICATION_JSON, order = 1)
    public void getSmartDevices(RoutingContext rc) {
        List<String> uri = rc.queryParam(SMART_DEVICE_URI);

        directoryRepository.getDirectory()
                .thenCompose(directory -> {
                    List<SmartDeviceId> smartDevicesId;

                    if (uri != null && !uri.isEmpty()) {
                        smartDevicesId = new ArrayList<>();
                        for (String string : uri) {
                            SmartDeviceId id = directory.getSmartDeviceId(string);
                            if (id != null) {
                                smartDevicesId.add(id);
                            } else if (log.isLoggable(Level.FINEST)) {
                                log.finest("Smartdevice with uri [" + string + "] not found");
                            }
                        }
                    } else {
                        smartDevicesId = directory.getSmartDevices();
                    }


                    List<CompletableFuture<SmartDevice>> futures = smartDevicesId.stream()
                            .map(id -> smartDeviceRepository.getById(id.getId()))
                            .collect(Collectors.toList());


                    return CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]))
                            .thenApply(v -> futures.stream()
                                    .map(CompletableFuture::join)
                                    .collect(Collectors.toList()));
                })
                .thenAccept(smartDevices -> {
                    rc.response().putHeader(CONTENT_TYPE, APP_CHARSET)
                            .setStatusCode(200)
                            .end(SmartDeviceJsonCodec.DevicestoJson(smartDevices));
                })
                .exceptionally(e -> {
                    if (e.getCause() instanceof NoSuchElementException) {
                        rc.response().setStatusCode(404).end();
                    } else if (e.getCause() instanceof IllegalArgumentException) {
                        rc.response().setStatusCode(400).end();
                    } else {
                        log.severe("Error retrieving smart devices: " + e.getMessage());
                        rc.response().setStatusCode(500).end();
                    }
                    return null;
                });
    }

    /**
     * get a smart device
     *
     * @param rc routing context
     */
    @Route(path = "/devices/:smartDeviceId", methods = Route.HttpMethod.GET, consumes = MediaType.APPLICATION_JSON, order = 3)
    public void getSmartDevice(RoutingContext rc) {
        String id = rc.pathParam(SMARTDEVICEID);

        smartDeviceRepository.getById(id)
                .thenAccept(smartDevice -> {
                    rc.response()
                            .putHeader(CONTENT_TYPE, APP_CHARSET)
                            .setStatusCode(200)
                            .end(SmartDeviceJsonCodec.toJson(smartDevice));
                })
                .exceptionally(e -> {
                    if (e.getCause() instanceof NoSuchElementException) {
                        rc.response().setStatusCode(404).end();
                    } else if (e.getCause() instanceof IllegalArgumentException) {
                        rc.response().setStatusCode(400).end();
                    } else {
                        log.severe("Error retrieving smart device: " + e.getMessage());
                        rc.response().setStatusCode(500).end();
                    }
                    return null;
                });
    }

    /**
     * Delete a smart device
     * @param rc routing context
     */
    @Route(path = "/devices/:smartDeviceId", methods = Route.HttpMethod.DELETE, order = 5)
    public void deleteSmartDevice(RoutingContext rc) {
        String creatorId = rc.user().principal().getString(USER_ID_STRING);
        ResourceName creator = new ResourceName(ResourceName.DOMAIN_SMARTDEVICE, SMART_DEVICE, creatorId);
        String id = rc.pathParam(SMARTDEVICEID);

        smartDeviceRepository.ifPresentOrElse(
                id,
                smartDevice -> {
                    String uri = smartDevice.getUri();


                    if (!modedegrade) {
                        try {
                            String encodedUri = URLEncoder.encode(uri, StandardCharsets.UTF_8);
                            HttpRequest externalApiRequest = HttpRequest.newBuilder()
                                    .uri(URI.create(externalApiUrl + "/" + encodedUri))
                                    .timeout(Duration.ofMinutes(2))
                                    .header(CONTENT_TYPE, "application/json")
                                    .DELETE()
                                    .build();

                            httpClient.sendAsync(externalApiRequest, HttpResponse.BodyHandlers.ofString())
                                    .thenApply(response -> {
                                        if (response.statusCode() >= 400) {
                                            System.err.println("Erreur de l'API externe: " + response.statusCode() + " - " + response.body());
                                        } else {
                                            System.out.println("Réponse de l'API externe : " + response.body());
                                        }
                                        return response;
                                    });
                        } catch (Exception e) {
                            log.warning("Error calling external API: " + e.getMessage());
                        }
                    }

                    try {
                        smartDevice.deleteSmartDevice(creator, eventPublisher);
                        rc.response().setStatusCode(204).end();
                    } catch (NoSuchElementException e) {
                        log.warning("Entity not found during deletion: " + e.getMessage());
                        rc.response().setStatusCode(404).end();
                    } catch (IllegalArgumentException e) {
                        log.warning("Invalid argument during deletion: " + e.getMessage());
                        rc.response().setStatusCode(400).end();
                    } catch (Exception e) {
                        log.severe("Unexpected error during deletion: " + e.getMessage());
                        rc.response().setStatusCode(500).end();
                    }
                },
                () -> {
                    log.finest("Smart device not found for deletion: " + id);
                    rc.response().setStatusCode(404).end();
                }
        );
    }

    /**
     * Create a Register
     * @param rc routing context
     */
    @Route(path = "/devices/:smartDeviceId/measures", methods = Route.HttpMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON, order = 7)
    public void postMeasure(RoutingContext rc) {
        String creatorId = rc.user().principal().getString(USER_ID_STRING);
        String smartDeviceId = rc.pathParam(SMARTDEVICEID);
        JsonObject body = rc.getBodyAsJson();
        String key = body.getString(SMART_DEVICE_KEY);
        String typeMeasure = body.getString(SMART_DEVICE_TYPE);
        String description = body.getString(SMART_DEVICE_DESCRIPTION);

        if (key == null) {
            key = UUID.randomUUID().toString();
        }

        final String finalKey = key;

        smartDeviceRepository.ifPresentOrElse(
                smartDeviceId,
                smartDevice -> {
                    try {
                        ResourceName creator = new ResourceName(ResourceName.DOMAIN_SMARTDEVICE, "smartDevice", creatorId);
                        smartDevice.createMeasure(finalKey, typeMeasure, description, creator, eventPublisher);

                        rc.response()
                                .putHeader("Location", rc.normalizedPath() + "/" + finalKey)
                                .putHeader(CONTENT_TYPE, APP_CHARSET)
                                .setStatusCode(201)
                                .end(RegisterJsonCodec.toJson(smartDevice, finalKey));
                    } catch (SecurityException e) {
                        log.warning("Security exception: " + e.getMessage());
                        rc.response().setStatusCode(403).end(e.getMessage());
                    } catch (Exception e) {
                        log.severe("Error creating measure: " + e.getMessage());
                        rc.response().setStatusCode(500).end();
                    }
                },
                () -> {
                    log.finest("Smart device not found: " + smartDeviceId);
                    rc.response().setStatusCode(404).end();
                }
        );
    }
    /**
     * Get a register
     * @param rc routing context
     */
    @Route(path = "/devices/:smartDeviceId/measures/:key", methods = Route.HttpMethod.GET, produces = MediaType.APPLICATION_JSON, order = 6)
    public void getMeasure(RoutingContext rc) {
        String smartDeviceId = rc.pathParam(SMARTDEVICEID);
        String key = rc.pathParam(SMART_DEVICE_KEY);

        smartDeviceRepository.ifPresentOrElse(
                smartDeviceId,
                smartDevice -> {
                    try {
                        Measure measure = smartDevice.getMeasure(key);
                        if (measure != null) {
                            rc.response()
                                    .putHeader(CONTENT_TYPE, APP_CHARSET)
                                    .setStatusCode(200)
                                    .end(RegisterJsonCodec.toJson(smartDevice, key));
                        } else {
                            rc.response().setStatusCode(404).end("Measure not found");
                        }
                    } catch (Exception e) {
                        log.severe("Error retrieving measure: " + e.getMessage());
                        rc.response().setStatusCode(500).end();
                    }
                },
                () -> {
                    log.finest("Smart device not found: " + smartDeviceId);
                    rc.response().setStatusCode(404).end();
                }
        );
    }
    /**
     * Update a register
     *
     * @param rc routing context
     */
    @Route(path = "/devices/:smartDeviceId/measures/:key", methods = Route.HttpMethod.PUT, consumes = MediaType.APPLICATION_JSON, order = 8)
    public void updateMeasure(RoutingContext rc) {
        String smartDeviceId = rc.pathParam(SMARTDEVICEID);
        String key = rc.pathParam(SMART_DEVICE_KEY);
        String creatorId = rc.user().principal().getString(USER_ID_STRING);
        ResourceName creator = new ResourceName(ResourceName.DOMAIN_SMARTDEVICE, SMART_DEVICE, creatorId);
        JsonObject measure = rc.getBodyAsJson();

        if (key == null || !key.equals(measure.getString(SMART_DEVICE_KEY))) {
            // TODO for the moment it is not possible to change the key... but it should be possible => juste get the old name from the query param and the new one from the body...
            rc.response().setStatusCode(400).end("Key in path and in body are not the same or key is null...");
            return;
        }

        smartDeviceRepository.ifPresentOrElse(
                smartDeviceId,
                smartDevice -> {
                    try {
                        smartDevice.updateMeasure(
                                key,
                                measure.getString(SMART_DEVICE_TYPE),
                                measure.getString(SMART_DEVICE_DESCRIPTION),
                                creator,
                                eventPublisher
                        );

                        rc.response()
                                .putHeader(CONTENT_TYPE, APP_CHARSET)
                                .setStatusCode(200)
                                .end(RegisterJsonCodec.toJson(smartDevice, key));
                    } catch (NoSuchElementException e) {
                        rc.response().setStatusCode(404).end("Measure not found");
                    } catch (IllegalArgumentException e) {
                        rc.response().setStatusCode(400).end();
                    } catch (Exception e) {
                        log.severe("Error updating measure: " + e.getMessage());
                        rc.response().setStatusCode(500).end();
                    }
                },
                () -> {
                    log.finest("Smart device not found: " + smartDeviceId);
                    rc.response().setStatusCode(404).end();
                }
        );
    }

    /**
     * Delete a register
     * @param rc routing context
     */
    @Route(path = "/devices/:smartDeviceId/measures/:key", methods = Route.HttpMethod.DELETE, order = 10)
    public void deleteMeasure(RoutingContext rc) {
        String creatorId = rc.user().principal().getString(USER_ID_STRING);
        String smartDeviceId = rc.pathParam(SMARTDEVICEID);
        String key = rc.pathParam(SMART_DEVICE_KEY);
        ResourceName creator = new ResourceName(ResourceName.DOMAIN_SMARTDEVICE, "smartDevice", creatorId);

        smartDeviceRepository.ifPresentOrElse(
                smartDeviceId,
                smartDevice -> {
                    try {
                        smartDevice.deleteMeasure(key, creator, eventPublisher);
                        rc.response().setStatusCode(204).end();
                    } catch (NoSuchElementException e) {
                        log.finest("Measure not found: " + key);
                        rc.response().setStatusCode(404).end();
                    } catch (IllegalArgumentException e) {
                        log.warning("Invalid argument: " + e.getMessage());
                        rc.response().setStatusCode(400).end();
                    } catch (Exception e) {
                        log.severe("Error deleting measure: " + e.getMessage());
                        rc.response().setStatusCode(500).end();
                    }
                },
                () -> {
                    log.finest("Smart device not found: " + smartDeviceId);
                    rc.response().setStatusCode(404).end();
                }
        );
    }

    public static void main(String[] var0) {
        Quarkus.run(var0);
    }

}

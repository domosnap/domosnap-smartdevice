# domosnap-smartdevice-management

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=com.domosnap.smartdevice%3Adomosnap-smartdevice&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.domosnap.smartdevice%3Adomosnap-smartdevice)

*   maven 3
*   OpenJDK 18
*   port used = 8080

## Build:

'mvn clean package'

## Quality:
'mvn sonar:sonar -Dsonar.host.url= -Dsonar.token='

## Modules:

*   [domain](https://gitlab.com/domosnap/domosnap-smartdevice/-/tree/master/domain "domain"): core business implementation about smartdevice domain.
*   [infra](https://gitlab.com/domosnap/domosnap-smartdevice/-/tree/master/infra "infra"): technical implementation (launcher, adapter (eventStore, ...), port (repository), etc..)


## Run

### With Docker
    docker login
    docker run registry.gitlab.com/domosnap/domosnap-smartdevice:[tag]

### With JVM
	
	mvn clean package
	cd infra/target
	java -Dquarkus.config.locations=../src/main/configuration/application.properties -jar smartdevice-infra-1.0-SNAPSHOT-runner.jar
	
	cd front
	ng serve 
	

### For RasperryPi k8s64 cluster
	

Register the token:

- Create key in gitlab (Settings->Repository->Deploy Tokens)
- On k8s cluster add the key:

    kubectl create secret docker-registry reg-secret-smartdevice --docker-server=registry.gitlab.com --docker-username='gitlab-ci-token' --docker-password='generated-token' -n domosnap-smartdevice

	
**On each deploy:**
	
    mvn clean package
    sudo docker login registry.gitlab.com
    sudo docker build -f Dockerfile_arm64v8 . -t registry.gitlab.com/domosnap/domosnap-smartdevice:arm64v8-latest
    sudo docker push registry.gitlab.com/domosnap/domosnap-smartdevice:arm64v8-latest
    sudo docker build -f Dockerfile_ui_arm64v8 . -t registry.gitlab.com/domosnap/domosnap-smartdevice/ui:arm64v8-latest
    sudo docker push registry.gitlab.com/domosnap/domosnap-smartdevice/ui:arm64v8-latest
    kubectl apply -f smartdevice-arm64v8.yaml
    kubectl create configmap smartdevice --from-file=config.json=./rest-adapter/src/main/configuration/production-configuration.json -n domosnap-smartdevice

    
Now reachable at: [here](https://smartdeviceui.arnauddegiuli.info)


For new project don't forget to register the token:
    - Create key in gitlab (Project->Settings->Repository->Deploy Tokens with a username and password)
    - On k8s cluster add the key:

    kubectl create secret docker-registry domosnap-secret --docker-server=registry.gitlab.com --docker-username='gitlab-deploy-token' --docker-password='generated-token'

[More info](https://kubernetes.io/fr/docs/tasks/configure-pod-container/pull-image-private-registry/ "More info")


Don't forget to use imagePullSecret in pod definition: 
    
    imagePullSecrets:
        - name: domosnap-secret


[API Documentation](https://gitlab.com/domosnap/domosnap-smartdevice/-/blob/master/rest-adapter/src/main/resources/swagger.yaml "API Documentation") 

openssl x509 -pubkey -noout -in <certificat.pem> > <fichier de sortie.pub>


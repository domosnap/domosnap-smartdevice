FROM eclipse-temurin:19-jre-jammy

WORKDIR /opt/smartdevice

COPY ./infra/target/smartdevice-infra-1.0-SNAPSHOT-runner.jar ./smartdevice-infra-1.0-SNAPSHOT-runner.jar
COPY ./infra/src/main/configuration/application.properties /app/config/

ENTRYPOINT ["java", "-Dquarkus.config.locations=/app/config/application.properties", "-Duser.dir=/tmp", "-jar", "smartdevice-infra-1.0-SNAPSHOT-runner.jar"]

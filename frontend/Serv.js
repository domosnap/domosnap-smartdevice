/* serv.js
 * this is for NodeJS
 * Raphaël Lopes
 * 06.06.2020
 */
const http = require('http');
const express = require('express');
const app = new express();
var path = require('path');

const hostname = '127.0.0.1';
const port = 3000;
//path to JS and CSS
app.use(express.static(path.join(__dirname, 'Controller')));
app.use(express.static(path.join(__dirname, 'vue/css')));
//path for server
app.get('/SmartDevice', function(request, response)
{
    response.sendFile(__dirname + '/vue/SmartDevices.html');
});
app.get('/', function(request, response)
{
    response.sendFile(__dirname + '/index.html');
});
//launch server
app.listen(port, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
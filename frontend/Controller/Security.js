/* Security.js
 * this is for keycloak handling in JS
 * Raphaël Lopes
 * 06.06.2020
 */
//this allow to remove or add keycloak to the project

var IS_KEYCLOAK_ON = true;
//keycloak setup
if (IS_KEYCLOAK_ON) {
    var keycloak = new Keycloak({
        url: 'http://keycloak.arnauddegiuli.info/',
        realm: 'master',
        clientId: 'smartdevice'
    });
    keycloak.init({
        onLoad: 'login-required'
    }).then(function (authenticated) {
        if (authenticated) {
            var token = keycloak.token
        }
    }).catch(function () {
        alert('failed to initialize');
    });
}
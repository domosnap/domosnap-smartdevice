//set debug token if Keycloak is off
if (!IS_KEYCLOAK_ON) {
    var token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiT2xpdmllciBEcmllc2JhY2giLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1CSVJmT3J5dThVYy9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBQS9BQ0hpM3JlSUVVd21VSldqbTNoWmlFRXZCNjlJb2N1OHN3L3M5Ni1jL3Bob3RvLmpwZyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9jdXJhbnRpLW12cCIsImF1ZCI6ImN1cmFudGktbXZwIiwiYXV0aF90aW1lIjoxNTYyMTUyMzU2LCJ1c2VyX2lkIjoibjVNSEcyVEx1cVliemM4QXJoNkx3VkxmaVp1MiIsInN1YiI6Im41TUhHMlRMdXFZYnpjOEFyaDZMd1ZMZmladTIiLCJpYXQiOjE1NjIxNTIzNTYsImVtYWlsIjoib2xpdmllci5kcmllc2JhY2hAY3VyYW50aS5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJnb29nbGUuY29tIjpbIjExNTI3NDUyODg1NjQ2NDY0NDA1MiJdLCJlbWFpbCI6WyJvbGl2aWVyLmRyaWVzYmFjaEBjdXJhbnRpLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6Imdvb2dsZS5jb20ifX0.OLgwhtHzjhVmJV9U_lVCWkyPlRem5tyK8BIdRw2y7nDQzAkoh9sj2O_7uND7D6qSSgW9FQyEpXSdHfXE-YVYO6yHR_IJAt7ATawVtl4krVuKevDGOC6aVHncbfvYENvs-ADCWKYQ6-teEwC99S7_1iwcD5okuH4BQ2aOQe4XW7-7qVdhDHw0Q3BzPe9wlEoVqS1vebmxXBW-Rgt4Z-1LcMzYKVm5YM2Uiw-vibxAFRD1QbM1pm-3uQYlFrpHy5ksiuh4YoaIeZKiJKZNOEOzJ-XmdtvYwIWmtjZ5gI2iol75kQNJ00A2aF7H2pN90v6DTTxvaAT67-Xoc8stYx1XUA";              
}

var protocol = "https://";
var back_end_host = "smartdevice.arnauddegiuli.info";
//var protocol = "http://";
//var back_end_host = "localhost:8080";

//Vue variable
var vue = new Vue({
    el: '#app',
    //variable for the vue
    data: {
        activeRegister: null,
        storedRegister: [],
        isAlreadyCreated: false,
        errorModel: "",
        error: ""
    },
    methods: {
        /**
         * open the modal when creating
         * @param {*} event 
         */
        PostOpenModal: function (event) {
            this.isAlreadyCreated = false;
        },
        /**
         * get a smart device
         * @param {*} event 
         */
        GetSmartDevice: function (event) {
            if (document.getElementById('SmartDeviceUID').value != "") {
                this.error = ""
                this.isAlreadyCreated = true;
                ResetModel();
                //verification for keycloak
                if (IS_KEYCLOAK_ON && keycloak.isTokenExpired()) {  
                    keycloak.updateToken().then(function () {
                        token = keycloak.token;
                        SmartDeviceGetRequest();
                    });
                } else {
                    SmartDeviceGetRequest();
                }
            } else {
                this.error = "Please enter value in the input field"
            }
        },
        /**
         * Create a smart device
         * @param {*} event 
         */
        PostSmartDevice: function (event) {
            if (document.getElementById('uri').value != "" && this.storedRegister.length > 0) {
                var SmartDevice = {
                    uri: document.getElementById('uri').value,
                    type: document.getElementById('type').value,
                    active: document.getElementById('active').checked,
                    measures: this.storedRegister
                }
                if (document.getElementById('idSmartDevice').value != "") {
                    SmartDevice.smartDeviceId = document.getElementById('idSmartDevice').value
                }
                //verification for keycloak
                if (IS_KEYCLOAK_ON && keycloak.isTokenExpired()) {               
                    keycloak.updateToken().then(function () {
                        SmartDevicesPostRequest(SmartDevice);
                    });
                } else {
                    SmartDevicesPostRequest(SmartDevice);
                }
                ResetModel();
                document.getElementById('SmartDeviceUID').value = document.getElementById('idSmartDevice').value;
                $('#smartDeviceModal').modal('hide')

            } else {
                this.errorModel = "Please fill all input for customer";
            }
        },
        /**
         * Create a register
         * @param {*} event 
         */
        PostRegister: function (event) {
            if (document.getElementById('key').value != "" && document.getElementById('typeMeasure').value != "" && document.getElementById('description').value != "") {
                this.errorModel = "";
                var register = {
                    key: document.getElementById('key').value,
                    type: document.getElementById('typeMeasure').value,
                    description: document.getElementById("description").value,
                }
                if (this.isAlreadyCreated) {
                    //verification for keycloak
                    if (IS_KEYCLOAK_ON && keycloak.isTokenExpired()) { 
                        keycloak.updateToken().then(function () {
                            token = keycloak.token;
                            RegisterPostRequest(register);
                        });
                    } else {
                        RegisterPostRequest(register);
                    }
                }
                this.storedRegister.push(register);
                document.getElementById('key').value = "";
                document.getElementById('typeMeasure').value = "";
                document.getElementById('description').value = "";
                $('#registerModal').modal('hide')
            } else {
                this.errorModel = "Please fill all input for contract";
            }
        },
        /**
         * Delete a smart device
         * @param {*} event 
         */
        DeleteSmartDevice: function (event) {
            $('#smartDeviceModal').modal('hide')
            //verification for keycloak
            if (IS_KEYCLOAK_ON && keycloak.isTokenExpired()) {   
                keycloak.updateToken().then(function () {
                    token = keycloak.token;
                    SmartDeviceDeletetRequest()
                });
            } else {
                SmartDeviceDeletetRequest();
            }
        },
        /**
         * delte a register
         * @param {int} id id of the partner
         * @param {string} key key of the register
         */
        DeleteRegister: function (id, key) {
            if (this.isAlreadyCreated) {
                //verification for keycloak
                if (IS_KEYCLOAK_ON && keycloak.isTokenExpired()) {
                    keycloak.updateToken().then(function () {
                        token = keycloak.token;
                        RegisterDeleteRequest();
                    });
                } else {
                    RegisterDeleteRequest();
                }

            }
            for (var i = 0; i < this.storedRegister.length; i++) {
                if (this.storedRegister[i].id == id && this.storedRegister[i].key == key) {
                    this.storedRegister.splice(i, 1);
                    break;
                }
            }
        }
    }
})
//if the modal close on find reset it
$(document).ready(function() {
    $('#smartDeviceModal').on('hidden.bs.modal', function (e) {
    if(vue._data["isAlreadyCreated"]){
        ResetModel();
    }
  })});
/**
 * reset the vue and variable
 */
function ResetModel() {
    vue._data["storedRegister"] = [];
    errorModel = "";
    document.getElementById('idSmartDevice').value = "";
    document.getElementById('uri').value = "";
    document.getElementById('active').outerHTML = '<input type="checkbox" id="active" value="active" checked>'
    document.getElementById('type').innerHTML = '<option value="TEST">TEST</option>';

}
/**
 * Request to create a smart device
 * @param {SmartDevice} SmartDevice 
 */
function SmartDevicesPostRequest(SmartDevice) {
    console.log(JSON.stringify(SmartDevice));
    fetch(protocol + back_end_host + '/devices', {
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + token,
                "Content-Type": "application/json; charset=UTF-8",
                "idPartner": "1"
            },
            body: JSON.stringify(SmartDevice)
        }).then(response => {
            if (response.status == "404") {
                return null;
            }
            return response.json()
        })
        .then(json => {
            document.getElementById('SmartDeviceUID').value = json["id"];
        }).catch(err => {
            console.log("in error case")
            console.log(err);
        });
}
/**
 * Request to create a register
 * @param {register} register regsiter to create
 */
function RegisterPostRequest(register) {
    fetch(protocol + back_end_host + '/devices/' + document.getElementById('SmartDeviceUID').value + "/measures", {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer ' + token,
            "Content-Type": "application/json; charset=UTF-8",
            "idPartner": "1"
        },
        body: JSON.stringify(register)
    }).catch(err => {
        console.log("in error case")
        console.log(err);
    });
}
/**
 * Request to get a smart device
 */
function SmartDeviceGetRequest() {
    fetch(protocol + back_end_host + '/devices/' + document.getElementById('SmartDeviceUID').value, {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + token,
            },
        })
        .then(response => {
            if (response.status == "404") {
                return null;
            }
            return response.json()
        })
        .then(json => {
            if (json != null) {
                $('#smartDeviceModal').modal('show')
                document.getElementById('idSmartDevice').value = json["id"];
                document.getElementById('uri').value = json["uri"];
                document.getElementById('type').innerHTML = '<option value="' + json["type"] + '">' + json["type"] + '</option>';
                if (json["active"] == "true") {
                    document.getElementById('active').outerHTML = '<input type="checkbox" id="active" value="active" onclick="return false;" checked>'
                } else {
                    document.getElementById('active').outerHTML = '<input type="checkbox" id="active" value="active" onclick="return false;">'
                }
                
                vue._data["storedRegister"] = json["measures"]

            } else {
                vue._data["error"] = "There is no Customer with that Customer UID"
            }
        }).catch(err => {
            console.log(err);
        });
}
/**
 * Request to delte a smart device
 */
function SmartDeviceDeletetRequest() {
    fetch(protocol + back_end_host + '/devices/' + document.getElementById('idSmartDevice').value, {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer ' + token,
        },
    });
}
/**
 * Request to delte a register
 */
function RegisterDeleteRequest() {
    fetch(protocol + back_end_host + '/devices/' + document.getElementById('SmartDeviceUID').value + "/measures/" + key, {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer ' + token,
            "idPartner": "1"
        },
    })
}
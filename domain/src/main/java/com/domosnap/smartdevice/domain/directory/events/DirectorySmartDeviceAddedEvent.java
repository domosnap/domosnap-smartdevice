/* DirectorySmartDeviceAddedEvent
 * event when a smart device is created for the directory
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.smartdevice.domain.directory.events;

import com.domosnap.smartdevice.domain.directory.DirectoryId;
import com.domosnap.smartdevice.domain.smartdevice.SmartDeviceId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class DirectorySmartDeviceAddedEvent extends EventBase<DirectoryId> {
	private SmartDeviceId smartDeviceId;
	private String uri;
	
	public DirectorySmartDeviceAddedEvent(DirectoryId directoryId, SmartDeviceId smartDeviceId, String uri, ResourceName creator) {
		super(directoryId, creator);
		this.smartDeviceId = smartDeviceId;
		this.uri = uri;
	}
	
	public SmartDeviceId getSmartDeviceId() {
		return this.smartDeviceId;
	}
	
	public String getUri() {
		return this.uri;
	}
}

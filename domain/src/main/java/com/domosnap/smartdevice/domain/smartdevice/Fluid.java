/* Fluid
 * enum for the fluid
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.smartdevice.domain.smartdevice;

public enum Fluid {
	UNKNOWN, WATER, ELECTRICITY, GAS
}

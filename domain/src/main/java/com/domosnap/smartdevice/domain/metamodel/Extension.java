package com.domosnap.smartdevice.domain.metamodel;

import java.util.ArrayList;
import java.util.List;

import com.domosnap.smartdevice.domain.metamodel.events.ExtensionCreatedEvent;
import com.domosnap.tools.core.services.cqrs.domain.Aggregate;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.domain.impl.ProjectionBase;

/**
 * Extension aggregate is here to make able to create new field to extends Smartdevice
 * @author adegiuli
 *
 */
public class Extension implements Aggregate<ExtensionId> {

	private ExtensionProjection projection;

	// QUERY
	@Override
	public ExtensionId getId() {
		return projection.id;
	}

	public SmartDeviceMetadataList getSmartDeviceFIeld() {
		return projection.smartDeviceMetadata;
	}

	class ExtensionProjection extends ProjectionBase {
		private MeasureMetadataList measureMetadata = new MeasureMetadataList();
		private SmartDeviceMetadataList smartDeviceMetadata = new SmartDeviceMetadataList();
		private ExtensionId id;
		private Integer idPartner;

		private List<Event<?>> history = new ArrayList<>();

		public ExtensionProjection(List<Event<?>> history) {
			register(ExtensionCreatedEvent.class, this::apply);
			history.forEach(this::apply);
		}
		
		public void apply(ExtensionCreatedEvent event) {
			this.id = event.getAggregateId();
			this.idPartner = event.getIdPartener();
			history.add(event);
		}
	}
}

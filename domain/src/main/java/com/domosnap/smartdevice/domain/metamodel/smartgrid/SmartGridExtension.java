package com.domosnap.smartdevice.domain.metamodel.smartgrid;

import com.domosnap.smartdevice.domain.metamodel.Extension;
import com.domosnap.smartdevice.domain.metamodel.MeasureField;
import com.domosnap.smartdevice.domain.metamodel.MeasureMetadataList;
import com.domosnap.smartdevice.domain.metamodel.SmartDeviceField;
import com.domosnap.smartdevice.domain.metamodel.SmartDeviceMetadataList;
import com.domosnap.smartdevice.domain.smartdevice.Fluid;

public class SmartGridExtension extends Extension {

	private MeasureMetadataList measureMetadata = new MeasureMetadataList();
	private SmartDeviceMetadataList smartDeviceMetadata = new SmartDeviceMetadataList();
	
	public SmartGridExtension() {
		measureMetadata.add(new MeasureField<String>("name", String.class, "valeur nom"));
		smartDeviceMetadata.add(new SmartDeviceField<>("fluid", Fluid.class));
	}
}

/* SmartDeviceCreatedEvent
 * Event for when a smart device is created
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.smartdevice.domain.smartdevice.events;

import com.domosnap.smartdevice.domain.metamodel.ExtensionId;
import com.domosnap.smartdevice.domain.smartdevice.SmartDeviceId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class SmartDeviceCreatedEvent extends EventBase<SmartDeviceId> {

	private final String uri;
	private final String type;
	private final Boolean active;
	private final Boolean virtual;
	private final ExtensionId extension;

    public SmartDeviceCreatedEvent(SmartDeviceId smartDeviceId, String uri, String type, Boolean active, Boolean virtual, ExtensionId extension, ResourceName creator) {
    	super(smartDeviceId, creator);
		this.uri = uri;
		this.type = type;
		this.active = active;
		this.virtual = virtual;
		this.extension = extension;
    }


	public String getUri() {
		return uri;
	}
	
	public String getType() {
		return type;
	}
	
	public Boolean getActive() {
		return active;
	}
	
	public Boolean getVirtual() {
		return virtual;
	}

	public ExtensionId getExtensionId() {
		return extension;
	}
}

/* SmartDeviceCreatedEvent
 * Event for when a smart device is created
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.smartdevice.domain.smartdevice.events;

import com.domosnap.smartdevice.domain.metamodel.ExtensionId;
import com.domosnap.smartdevice.domain.smartdevice.SmartDeviceId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class SmartDeviceUpdateFieldMetaDataEvent extends EventBase<SmartDeviceId> {

	private final ExtensionId extensionId;
	private final String extensionFieldName;
	private final Object value;

    public SmartDeviceUpdateFieldMetaDataEvent(SmartDeviceId smartDeviceId, ExtensionId extensionId, String extensionFieldName, Object value, ResourceName creator) {
    	super(smartDeviceId, creator);
		this.extensionId = extensionId;
		this.extensionFieldName = extensionFieldName;
		this.value = value;
    }


	public ExtensionId getExtensionId() {
		return extensionId;
	}
	
	public String getExtensionFieldName() {
		return extensionFieldName;
	}
	
	public Object getValue() {
		return value;
	}
}

/* CustomerDeleteEvent
 * Event for when a customer is deleted
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.smartdevice.domain.smartdevice.events;

import com.domosnap.smartdevice.domain.smartdevice.SmartDeviceId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class SmartDeviceDeleteEvent extends EventBase<SmartDeviceId> {

    public SmartDeviceDeleteEvent(SmartDeviceId smartDeviceId, ResourceName creator) {
    	super(smartDeviceId, creator);
    }
}

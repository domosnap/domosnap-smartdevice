/* SmartDeviceID
 * aggregate of the smart device
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.smartdevice.domain.smartdevice;

import java.util.UUID;

import com.domosnap.tools.core.services.cqrs.domain.AggregateId;

public class SmartDeviceId implements AggregateId {
    private String value;

    public SmartDeviceId(String value) {

        this.value = value;
    }

    public static SmartDeviceId generate() {
        return new SmartDeviceId(UUID.randomUUID().toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmartDeviceId messageId = (SmartDeviceId) o;

        return !(value != null ? !value.equals(messageId.value) : messageId.value != null);

    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        return value;
    }

	@Override
	public String getId() {
		return value;
	}
}

	/* SmartDevice
 * This is the domain for the Smart Device
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.smartdevice.domain.smartdevice;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.domosnap.smartdevice.domain.metamodel.Extension;
import com.domosnap.smartdevice.domain.metamodel.ExtensionId;
import com.domosnap.smartdevice.domain.smartdevice.events.SmartDeviceChangeEvent;
import com.domosnap.smartdevice.domain.smartdevice.events.SmartDeviceCreateMeasureEvent;
import com.domosnap.smartdevice.domain.smartdevice.events.SmartDeviceCreatedEvent;
import com.domosnap.smartdevice.domain.smartdevice.events.SmartDeviceDeleteEvent;
import com.domosnap.smartdevice.domain.smartdevice.events.SmartDeviceDeleteMeasureEvent;
import com.domosnap.smartdevice.domain.smartdevice.events.SmartDeviceUpdateMeasureEvent;
import com.domosnap.tools.core.services.cqrs.domain.Aggregate;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.domain.impl.ProjectionBase;
import com.domosnap.tools.core.services.exception.AlreadyExistingElementException;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class SmartDevice implements Aggregate<SmartDeviceId> {

	private Logger log = Logger.getLogger(SmartDevice.class.getName());

	private final static  String MEASURE_NAME = "Measure named [";

	private SmartDeviceProjection projection;

	/**
	 * Create a smart device
	 * @param smartDeviceId smart device id
	 * @param uri uri of the smart device
	 * @param type type of smart device
	 * @param active is the smart device active
	 * @param virtual true if the smart device is virtual
	 * @param principal resource name
	 * @param eventPublisher consumer
	 */
	public static void create(SmartDeviceId smartDeviceId, String uri, String type,
			Boolean active, Boolean virtual, Extension extension, ResourceName principal, Consumer<Event<?>> eventPublisher) {
		
		ExtensionId id = ExtensionId.generate(); // TODO extension.getId();
		
		eventPublisher
				.accept(new SmartDeviceCreatedEvent(smartDeviceId, uri, type, active, virtual, id, principal));
	}
	
	public SmartDevice(List<Event<?>> history) {
		projection = new SmartDeviceProjection(history);
	}

	/**
	 * Change the smart device value

	 * @param uri uri of the smart device
	 * @param type type of smart device
	 * @param active is the smart device active
	 * @param principal resource name
	 * @param eventPublisher consumer
	 */
	public void updateSmartDevice(String uri, String type, Boolean active,
			ResourceName principal, Consumer<Event<?>> eventPublisher) {

		Event<SmartDeviceId> event = new SmartDeviceChangeEvent(getId(), uri, type, active,
				principal);
		projection.apply(event);
		eventPublisher.accept(event);
	}

	/**
	 * Create a new register
	 * @param key key of the register 
	 * @param type type of the measure
	 * @param description description of the register
	 * @param fluid type of the fluid
	 * @param principal resource name
	 * @param eventPublisher consumer
	 */
	public void createMeasure(String key, String type, String description,
			ResourceName principal, Consumer<Event<?>> eventPublisher) {

			for (Measure measure : projection.measureList) {
				if (measure.getKey().equals(key)) {
					// Already exist
					log.log(Level.FINEST, "Measure [{0}] already existing for user [{1}].", new Object[] {key, getId()});
					throw new AlreadyExistingElementException( MEASURE_NAME.concat(key).concat("]."));
				}
			}
			
			Event<SmartDeviceId> event = new SmartDeviceCreateMeasureEvent(getId(), key, type,
					description, principal);
			projection.apply(event);
			eventPublisher.accept(event);

	}

	/**
	 * Update a register
	 * @param key key of the register 
	 * @param type type of the measure
	 * @param description description of the register
	 * @param fluid type of the fluid
	 * @param principal resource name
	 * @param eventPublisher consumer
	 */
	public void updateMeasure(String key, String type, String description,
			ResourceName principal, Consumer<Event<?>> eventPublisher) {

			for (Measure measure : projection.measureList) {
				if (measure.getKey().equals(key)) {
					Event<SmartDeviceId> event = new SmartDeviceUpdateMeasureEvent(getId(), key, type,
							description, principal);
					projection.apply(event);
					eventPublisher.accept(event);
					return;
				}
			}
			
			log.log(Level.FINEST, "Measure [{0}] already existing for smartdevice [{1}].", new Object[] {key, getId()});
			throw new NoSuchElementException( MEASURE_NAME.concat(key).concat("] not found to update."));

	}
	
	/**
	 * delete a register
	 * @param key key of the register
	 * @param principal resource name
	 * @param eventPublisher consumer
	 */
	public void deleteMeasure(String key, ResourceName principal,
			Consumer<Event<?>> eventPublisher) {
		

			for (Measure measure : projection.measureList) {
				if (measure.getKey().equals(key)) {
					Event<SmartDeviceId> event = new SmartDeviceDeleteMeasureEvent(getId(), key, principal);
					projection.apply(event);
					eventPublisher.accept(event);
					return;
				}
			}

			log.log(Level.FINEST, "Impossible to remove measure [{0}, {1}] since it doesn't exist for customer [{2}].", new Object[] { key, getId()});
			throw new NoSuchElementException( MEASURE_NAME.concat( key).concat("] not found to delete."));
		}

	/**
	 * Delete a smart device
	 * @param principal resource name
	 * @param eventPublisher consumer
	 */
	public void deleteSmartDevice(ResourceName principal, Consumer<Event<?>> eventPublisher) {
		Event<SmartDeviceId> event = new SmartDeviceDeleteEvent(getId(), principal);
		projection.apply(event);
		eventPublisher.accept(event);
	}

	public void updateMetadataField(ExtensionId extensionId, String extensionFieldName, Object value, ResourceName principal, Consumer<Event<?>> eventPublisher) {

			if (projection.extension.getId().equals(extensionId)) {
				// TODO revoir extension: ca ne peut pas être un aggregat!!!!
//			for (SmartDeviceField<?> field : projection.extension.getSmartDeviceFIeld()) {
//				
//				
//				if (field.getName().equals(extensionFieldName)) {
//					if (field.getClazz().equals(value.getClass())) {
//					Event<SmartDeviceId> event = new SmartDeviceUpdateFieldMetaDataEvent(getId(), extensionId, extensionFieldName, value, principal);
//					projection.apply(event);
//					eventPublisher.accept(event);
//					return;
//					} else {
//						// TODO log error
//					}
//				}
//			}
			} else {
				log.log(Level.FINEST, "Bad EXTENSION"); // TODO update message
			}
			
			
//			log.log(Level.FINEST, "Measure [{0}] already existing for smartdevice [{1}].", new Object[] {key, getId()});
//			throw new NoSuchElementException("Measure named [".concat(key).concat("] not found to update."));

	}
	// QUERY
	public SmartDeviceId getId() {
		return projection.id;
	}


	public String getUri() {
		return projection.uri;
	}

	public String getType() {
		return projection.type;
	}

	public Boolean isActive() {
		return projection.active;
	}

	public Boolean isVirtual() {
		return projection.virtual;
	}

	public Boolean getDeleted() {
		return projection.deleted;
	}

	public List<Measure> getMeasureList() {
		return projection.measureList;
	}
	
// TODO Revoir extension: ca ne peut pas etre un aggregat!
//	public List<SmartDeviceField<?>> getMetaData() {
//		return projection.extension.getSmartDeviceFIeld(); // metadata
//	}

	public Measure getMeasure(String key) {
		for (Measure measure : projection.measureList) {
			if (key.equals(measure.getKey())) {
				return measure;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{\"id\":\"").append(getId()).append("\",")
				.append("\"uri\":\"").append(getUri()).append("\",").append("\"type\":\"").append(getType()).append("\",")
				.append("\"active\":\"").append(isActive()).append("\",\"virtual\":\"").append(isVirtual()).append("\"}");
		return sb.toString();
	}

	class SmartDeviceProjection extends ProjectionBase {
		private List<Measure> measureList = new ArrayList<>();
		private SmartDeviceId id;
		private String uri;
		private String type;
		private Boolean active = Boolean.FALSE;
		private Boolean virtual = Boolean.FALSE;
		private Boolean deleted = Boolean.FALSE;
		private ExtensionId extension;
		private List<Event<?>> history = new ArrayList<>();

		public SmartDeviceProjection(List<Event<?>> history) {
			register(SmartDeviceCreatedEvent.class, this::apply);
			register(SmartDeviceChangeEvent.class, this::apply);
			register(SmartDeviceDeleteEvent.class, this::apply);
			register(SmartDeviceCreateMeasureEvent.class, this::apply);
			register(SmartDeviceUpdateMeasureEvent.class, this::apply);
			register(SmartDeviceDeleteMeasureEvent.class, this::apply);
			history.forEach(this::apply);
		}

		public void apply(SmartDeviceDeleteMeasureEvent event) {
			for (Measure measure : measureList) {
        		if (event.getKey().equals(measure.getKey())) {
        			measureList.remove(measure);
        			break;
        		}
    		}
        	history.add(event);
		}

		public void apply(SmartDeviceCreateMeasureEvent event) {
			measureList.add(new Measure(event.getKey(), event.getType(),
					event.getDescription()));
		}

		public void apply(SmartDeviceUpdateMeasureEvent event) {
			for (Measure measure : measureList) {
				if (measure.getKey().equals(event.getKey())) {
					measure.setDescription(event.getDescription());
					measure.setType(event.getType());
					break;
				}
			}
		}
		
		public void apply(SmartDeviceCreatedEvent event) {
			this.id = event.getAggregateId();
			this.uri = event.getUri();
			this.type = event.getType();
			this.active = event.getActive();
			this.virtual = event.getVirtual();
			this.extension = event.getExtensionId();
			history.add(event);
		}

		public void apply(SmartDeviceChangeEvent event) {

			if (event.getUri() != null) {
				this.uri = event.getUri();
			}
			if (event.getType() != null) {
				this.type = event.getType();
			}
			if (event.getActive() != null) {
				this.active = event.getActive();
			}
			history.add(event);
		}

		public void apply(SmartDeviceDeleteEvent event) {
			deleted = true;
			history.add(event);
		}

	}

}

/* SmartDeviceChangeEvent
 * Event for when a smart device change
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.smartdevice.domain.smartdevice.events;

import com.domosnap.smartdevice.domain.smartdevice.SmartDeviceId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class SmartDeviceChangeEvent extends EventBase<SmartDeviceId> {


	private final String uri;
	private final String type;
	private final Boolean active;
	
	public SmartDeviceChangeEvent(SmartDeviceId smartDeviceId, String uri, String type, Boolean active, ResourceName creator) {
		super(smartDeviceId, creator);

		this.uri = uri;
		this.type = type;
		this.active = active;
	}

	public String getUri() {
		return uri;
	}
	
	public String getType() {
		return type;
	}
	
	public Boolean getActive() {
		return active;
	}
}

/* Register
 * this is the domain for the register
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.smartdevice.domain.smartdevice;

import java.util.List;

import com.domosnap.smartdevice.domain.metamodel.MeasureField;

public class Measure {
	
	private String key;
	private String type;
	private String description;
	
	public Measure(String key,String type,String description) {
		this.key = key;
		this.type = type;
		this.description = description;
	}
	
	public String getKey() {
		return key;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public List<MeasureField<?>> getMetaData() {
		return null; // TODO metadata
	}
}

/* DirectoryCreatedEvent
 * event when a directory is created
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.smartdevice.domain.directory.events;

import com.domosnap.smartdevice.domain.directory.DirectoryId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class DirectoryCreatedEvent extends EventBase<DirectoryId> {

	public DirectoryCreatedEvent(DirectoryId directoryId, ResourceName creator) {
		super(directoryId, creator);
	}
}

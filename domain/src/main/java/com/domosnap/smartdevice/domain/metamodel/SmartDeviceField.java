package com.domosnap.smartdevice.domain.metamodel;

public class SmartDeviceField<T> {

	private String name;
	private Class<T> clazz;
	private T value;
	
	public SmartDeviceField(String name, Class<T> clazz, T value) {
		this.name = name;
		this.clazz = clazz;
		this.value = value;
	}
	
	public SmartDeviceField(String name, Class<T> clazz) {
		this.name = name;
		this.clazz = clazz;
	}

	public Class<T> getClazz() {
		return clazz;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}
}

/* SmartDeviceCreatedEvent
 * Event for when a smart device is created
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.smartdevice.domain.metamodel.events;

import com.domosnap.smartdevice.domain.metamodel.ExtensionId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class ExtensionCreatedEvent extends EventBase<ExtensionId> {

	private final Integer idPartner;

	public ExtensionCreatedEvent(ExtensionId extensionId, Integer idPartner, ResourceName creator) {
		super(extensionId, creator);
		this.idPartner = idPartner;
	}

	public Integer getIdPartener() {
		return idPartner;
	}
}

/* DirectorySmartDeviceAddedEvent
 * event when a smart device is deleted for the directory
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.smartdevice.domain.directory.events;

import com.domosnap.smartdevice.domain.directory.DirectoryId;
import com.domosnap.smartdevice.domain.smartdevice.SmartDeviceId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class DirectorySmartDeviceRemovedEvent extends EventBase<DirectoryId> {
	private SmartDeviceId smartDeviceId;
	public DirectorySmartDeviceRemovedEvent(DirectoryId directoryId,SmartDeviceId smartDeviceId, ResourceName creator) {
		super(directoryId, creator);
		this.smartDeviceId = smartDeviceId;
	}
	public SmartDeviceId getSmartDeviceId() {
		return this.smartDeviceId;
	}
}

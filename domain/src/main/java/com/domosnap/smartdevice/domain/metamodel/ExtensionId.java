/* SmartDeviceID
 * aggregate of the smart device
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.smartdevice.domain.metamodel;

import java.util.UUID;

import com.domosnap.tools.core.services.cqrs.domain.AggregateId;

public class ExtensionId implements AggregateId {
    private String value;

    public ExtensionId(String value) {

        this.value = value;
    }

    public static ExtensionId generate() {
        return new ExtensionId(UUID.randomUUID().toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExtensionId messageId = (ExtensionId) o;

        return !(value != null ? !value.equals(messageId.value) : messageId.value != null);

    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        return value;
    }

	@Override
	public String getId() {
		return value;
	}
}

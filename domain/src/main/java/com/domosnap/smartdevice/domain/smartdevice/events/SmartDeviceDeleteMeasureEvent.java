/* SmartDeviceDeleteRegisterEvent
 * Event for when a Register is deleted
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.smartdevice.domain.smartdevice.events;

import com.domosnap.smartdevice.domain.smartdevice.SmartDeviceId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class SmartDeviceDeleteMeasureEvent extends EventBase<SmartDeviceId> {

	private final String key;

    public SmartDeviceDeleteMeasureEvent(SmartDeviceId smartDeviceId, String key, ResourceName creator) {
    	super(smartDeviceId, creator);
        this.key = key;
    }

    public String getKey() {
		return key;
	}
}
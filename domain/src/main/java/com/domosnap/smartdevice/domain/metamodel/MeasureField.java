package com.domosnap.smartdevice.domain.metamodel;

public class MeasureField<T> extends SmartDeviceField<T> {

	public MeasureField(String name, Class<T> clazz, T value) {
		super(name, clazz, value);
	}
	
	public MeasureField(String name, Class<T> clazz) {
		super(name, clazz);
	}

}

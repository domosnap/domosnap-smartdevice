package com.domosnap.smartdevice.domain.directory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.domosnap.smartdevice.domain.directory.events.DirectoryCreatedEvent;
import com.domosnap.smartdevice.domain.directory.events.DirectorySmartDeviceAddedEvent;
import com.domosnap.smartdevice.domain.directory.events.DirectorySmartDeviceRemovedEvent;
import com.domosnap.smartdevice.domain.smartdevice.SmartDeviceId;
import com.domosnap.tools.core.services.cqrs.domain.Aggregate;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.domain.impl.ProjectionBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class Directory implements Aggregate<DirectoryId> {

	private Logger log = Logger.getLogger(Directory.class.getName());
	
	public final static DirectoryId mainDirectory = new DirectoryId("0");
	
    private DirectoryProjection projection;
    /**
     * This will create a directory
     * @param creator resource name
     * @param dispatchEvent cosumer
     */
    public static void create(ResourceName creator, Consumer<Event<?>> dispatchEvent) {
    	dispatchEvent.accept(new DirectoryCreatedEvent(mainDirectory, creator));
    }
    
    public Directory(List<Event<?>> history) {
       projection = new DirectoryProjection(history);
    }
    /**
     * this will append when a user is added
     * @param smartDeviceId the smart device ID
     * @param creator resource name
     * @param dispatchEvent consumer
     */
    public void addSmartDevice(SmartDeviceId smartDeviceId, String uri, ResourceName creator, Consumer<Event<?>> dispatchEvent) {
    	Event<DirectoryId> event = new DirectorySmartDeviceAddedEvent(getId(), smartDeviceId, uri, creator);
    	projection.apply(event);
    	dispatchEvent.accept(event);
    }
    
    /**
     * this will append when a user is removed
     * @param smartDeviceId the smart device ID
     * @param creator resource name
     * @param dispatchEvent consumer
     */
    public void removeSmartDevice(SmartDeviceId smartDeviceId, ResourceName creator, Consumer<Event<?>> dispatchEvent) {
    	Event<DirectoryId> event = new DirectorySmartDeviceRemovedEvent(getId(), smartDeviceId, creator);
    	if (projection.smartDeviceById.containsKey(smartDeviceId)) {
    		projection.apply(event);
    		dispatchEvent.accept(event);
    	} else {
    		log.log(Level.FINEST, "Impossible to remove smartdevice [{0}] since it isn''t contain in directory [{1}].", new Object[] {smartDeviceId, getId()});
    		throw new NoSuchElementException("No smartdevice [".concat(String.valueOf(smartDeviceId)).concat("] to delete."));
    	}
    }
    
    public DirectoryId getId() {
        return projection.id;
    }

    public List<SmartDeviceId> getSmartDevices() {
    	return Collections.unmodifiableList(new ArrayList<SmartDeviceId>(projection.smartDeviceById.keySet()));
    }
    
    public SmartDeviceId getSmartDeviceId(String uri) {
    	return projection.smartDeviceByUri.get(uri);
    }
    
    private class DirectoryProjection extends ProjectionBase {

    	public DirectoryId id;
    	public Map<SmartDeviceId, String> smartDeviceById = new HashMap<>();
    	public Map<String, SmartDeviceId> smartDeviceByUri = new HashMap<>();
    	
        public List<Event<?>> history = new ArrayList<>();

        public DirectoryProjection(List<Event<?>> history) {
            register(DirectoryCreatedEvent.class, this::apply);
            register(DirectorySmartDeviceAddedEvent.class, this::apply);
            register(DirectorySmartDeviceRemovedEvent.class, this::apply);
            history.forEach(this::apply);
        }

        public void apply(DirectoryCreatedEvent event) {
        	this.id = event.getAggregateId();
        	history.add(event);
        }
        
        public void apply(DirectorySmartDeviceAddedEvent event) {
        	smartDeviceById.put(event.getSmartDeviceId(), event.getUri());
        	smartDeviceByUri.put(event.getUri(), event.getSmartDeviceId());
        	history.add(event);
        }
        
        public void apply(DirectorySmartDeviceRemovedEvent event) {
        	SmartDeviceId smartDeviceId = event.getSmartDeviceId();
        	String uri = smartDeviceById.get(smartDeviceId);
        	smartDeviceById.remove(smartDeviceId);
        	smartDeviceByUri.remove(uri);
        	history.add(event);
        }
    }
}
package com.domosnap.smartdevice.domain.smartdevice.events;

import com.domosnap.smartdevice.domain.smartdevice.SmartDeviceId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class SmartDeviceUpdateMeasureEvent extends EventBase<SmartDeviceId> {

	private String key;
	private String description;
	private String type;

    public SmartDeviceUpdateMeasureEvent(SmartDeviceId smartDeviceId, String key, String type, String description, ResourceName creator) {
    	super(smartDeviceId, creator);
        this.key = key;
        this.description = description;
        this.type = type;
    }

	public String getKey() {
		return key;
	}

	public String getDescription() {
		return description;
	}
	
	public String getType() {
		return type;
	}
}
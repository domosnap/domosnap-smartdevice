Cypress.Commands.add('kcLoginAndRedirect', (username, password, redirectUrl) => {
  const kcRoot = 'https://keycloak.arnauddegiuli.info';
  const kcRealm = 'domosnap';
  const kcClient = 'smartdevice';
  const kcRedirectUri = 'http://localhost:4200/';

  const loginPageRequest = `${kcRoot}/realms/${kcRealm}/protocol/openid-connect/auth?client_id=${kcClient}&redirect_uri=${encodeURIComponent(kcRedirectUri)}&state=${createUUID()}&response_mode=fragment&response_type=code&scope=openid&nonce=${createUUID()}`;

  return cy.request({
    url: loginPageRequest,
    followRedirect: false
  })
  .then(response => {
    cy.log('Login page response', response);

    const html = document.createElement('html');
    html.innerHTML = response.body;
    const loginForm = html.querySelector('form');

    cy.log('Login form:', loginForm);

    if (!loginForm) {
      cy.log('No login form found, user might already be logged in.');
      return;
    }

    return cy.request({
      method: 'POST',
      url: loginForm.action,
      followRedirect: false,
      form: true,
      body: {
        username: username,
        password: password,
      }
    });
  })
  .then(response => {
    const locationHeader = response.headers['location'];

    if (!locationHeader) {
      throw new Error('Location header not found');
    }

    cy.log("locationHeader" + locationHeader);

    const params = new URLSearchParams(locationHeader.split('#')[1]);
    const code = params.get('code');

    cy.log("response code " + code);

    return cy.request({
      method: 'POST',
      url: `${kcRoot}/realms/${kcRealm}/protocol/openid-connect/token`,
      form: true,
      body: {
        grant_type: 'authorization_code',
        client_id: kcClient,
        redirect_uri: kcRedirectUri,
        code: code
      }
    });
  })
  .then(response => {
    const { access_token } = response.body;
    window.localStorage.setItem('kc_token', access_token);
    cy.log(access_token);
    cy.visit(redirectUrl, {
      onBeforeLoad: (win) => {
        win.localStorage.setItem('kc_token', access_token);
      }
    });
  });

  function createUUID() {
    var s = [];
    var hexDigits = '0123456789abcdef';
    for (var i = 0; i < 36; i++) {
      s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = '4';
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    s[8] = s[13] = s[18] = s[23] = '-';
    return s.join('');
  }
});


describe('Composant Device', () => {

  // Simuler une base de données locale
  let mockDatabase = {
    smartDevices: [],
    measures: [],
  };

beforeEach(() => {

cy.kcLoginAndRedirect('bastien', 'Bastien74150.', 'http://localhost:4200');

    // Réinitialiser les données simulées avant chaque test
    mockDatabase = {
      smartDevices: [],
      measures: [],
    };

    // Mock de la requête POST pour créer un smart device
    cy.intercept('POST', '/devices', (req) => {
      const newDevice = req.body;
      newDevice.id = newDevice.id || 'mock-' + Math.random().toString(36).substr(2, 9);
      mockDatabase.smartDevices.push(newDevice);

      req.reply({
        statusCode: 201,
        body: newDevice,
      });
    }).as('createSmartDevice');

    // Mock de la requête GET pour récupérer les smart devices
        cy.intercept('GET', '/devices', (req) => {
          req.reply({
            statusCode: 200,
            body: mockDatabase.smartDevices,
          });
        }).as('getSmartDevices');

    cy.intercept('PUT', '/devices/*', (req) => {
      const updatedDevice = req.body;
      const deviceId = updatedDevice.id; // L'ID est dans le corps de la requête

      if (!deviceId) {
        req.reply({
          statusCode: 400,
          body: { error: 'Device ID is missing in the request body' },
        });
        return;
      }

      // Trouver le smart device dans le backend simulé
      const deviceIndex = mockDatabase.smartDevices.findIndex((device) => device.id === deviceId);

      if (deviceIndex !== -1) {
        // Mise à jour des données générales du smart device
        const existingDevice = mockDatabase.smartDevices[deviceIndex];
        // Mettre à jour le smart device avec les nouvelles données
        mockDatabase.smartDevices[deviceIndex] = {
          id: existingDevice.id,
          name: updatedDevice.name || existingDevice.name,
          type: updatedDevice.type || existingDevice.type,
          uri: updatedDevice.uri || existingDevice.uri,
          active: updatedDevice.active !== undefined ? updatedDevice.active : existingDevice.active,
          measures: updatedDevice.measures ? updatedDevice.measures : existingDevice.measures,
        };

        req.reply({
          statusCode: 200,
          body: mockDatabase.smartDevices[deviceIndex],
        });
      } else {
        req.reply({
          statusCode: 404,
          body: { error: 'Smart device not found' },
        });
      }
    }).as('updateSmartDevice');


  });

  it('crée un nouveau smart device avec un ID unique', () => {

      cy.get('.btn.btn-primary').click();

      const id = 'test-' + Math.random().toString(36).substr(2, 9);

          cy.get('#idSmartDevice').type(id);
          cy.get('#uri').type('test-uri');
          // Nouveau code pour le mat-select
          cy.get('mat-select[name="type"]').click();
          cy.get('mat-option').contains('Light').click();
          cy.get('#active').check();
          cy.get('.modal-footer .btn-primary').click();

        // Attendre la requête POST pour le smart device
          cy.wait('@createSmartDevice').then((interception) => {
            // Vérifier que le smart device a bien été créé
            expect(interception.response.statusCode).to.eq(201);
            expect(mockDatabase.smartDevices).to.have.length(1);
            expect(mockDatabase.smartDevices[0]).to.deep.include({
              id,
              uri: 'test-uri',
              type: 'Light',
              active: true,
            });
          });

//        cy.request('/devices').then((response) => {
//            expect(response.status).to.eq(200);
//            expect(response.body).to.be.an('array');
//            expect(response.body).to.deep.include({
//              id,
//              uri: 'test-uri',
//              type: 'test-type',
//              active: true,
//            });
//          });

      // Ajouter une measures
      cy.get('.btn.btn-secondary:first').click();
      cy.get('button.btn.btn-primary').contains('+').click();

      cy.get('tbody[formArrayName="measures"]').within(() => {
          // S'assurer que les éléments du tableau sont rendus
          cy.get('tr').each((row, index) => {
          // Remplir le champ "Key"
          cy.wrap(row).find('input[formControlName="key"]').type(`key${index}`);

          // Remplir le champ "Type"
          cy.wrap(row).find('input[formControlName="type"]').type(`type${index}`);

          // Remplir le champ "Description"
          cy.wrap(row).find('input[formControlName="description"]').type(`description${index}`);
      });
    });

    cy.get('.modal-footer .btn-primary').click();

    // Vérifier que la mise à jour a été effectuée
    cy.wait('@updateSmartDevice').then((interception) => {
      // Assurez-vous que la réponse est correcte
      expect(interception.response.statusCode).to.eq(200);

      const updatedDevice = interception.response.body;

      // Valider les mesures dans le smart device mis à jour
      updatedDevice.measures.forEach((measure, index) => {
        expect(measure.key).to.eq(`key${index}`);
        expect(measure.type).to.eq(`type${index}`);
        expect(measure.description).to.eq(`description${index}`);
      });

  });
});
});





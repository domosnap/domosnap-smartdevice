export const environment = {
    production: true,
    configFile: 'assets/config/config.prod.json',
    backendUrl: 'http://localhost:8081/devices',
    backendUrlEngineApi: 'http://localhost:8085/devices'
  };

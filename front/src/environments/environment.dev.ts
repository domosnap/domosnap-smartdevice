export const environment = {
    production: false,
    configFile: 'assets/config/config.dev.json',
    backendUrl: 'http://localhost:8081/devices',
    backendUrlEngineApi: 'http://localhost:8085/devices'
  };

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SmartDevice } from '../model/smartdevice.model';
import { Measure } from '../model/measure.model';
import { environment } from '../../../environments/environment';

@Injectable({
providedIn: 'root'
})

export class SmartDeviceService {
    constructor(private http: HttpClient) {}

    adress: string = environment.backendUrl;


    list(): Observable<Array<SmartDevice>> {
        // return [{ uri: 'London' }];

        var headers = new HttpHeaders({
            'Content-Type': 'application/json',
          });
         //
        return this.http.get<Array<SmartDevice>>(this.adress, { headers: headers, responseType: 'json', observe: 'body'});
    }


    PostSmartDevice(smartDevice: SmartDevice): Observable<SmartDevice> {
        //console.log('Data to be sent:', smartDevice);
        var headers = new HttpHeaders({
                    'Content-Type': 'application/json',
                  });

        return this.http.post<SmartDevice>(this.adress, smartDevice, { headers: headers, responseType: 'json', observe: 'body'});
    }

    PutSmartDevice(smartDevice: SmartDevice, uid?: string ):Observable<SmartDevice> {
        var headers = new HttpHeaders({
                        'Content-Type': 'application/json',
                      });

     return this.http.put<SmartDevice>(this.adress + "/" + uid, smartDevice ,{ headers: headers, responseType: 'json', observe: 'body'});
    }

    delete(uid?: string): Observable<Object> {
        // TODO replace uid? => with an ID!!!

        var headers = new HttpHeaders({
            'Content-Type': 'application/json',
          });

        return this.http.delete<SmartDevice>(this.adress + "/" + uid , { headers: headers, responseType: 'json', observe: 'body'});
    }

}

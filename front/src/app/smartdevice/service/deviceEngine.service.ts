import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SmartDevice } from '../model/smartdevice.model';
import { environment } from '../../../environments/environment';

@Injectable({
providedIn: 'root'
})

export class DeviceService {
    constructor(private http: HttpClient) {}

    adress: string = environment.backendUrlEngineApi;

    getStatusLight(uid?: string): Observable<{status: String}> {
           return this.http.get<{status: String}>(this.adress + "/" + uid + "/STATUS");
    }

}

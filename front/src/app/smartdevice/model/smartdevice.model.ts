import {Measure} from "./measure.model"

export class SmartDevice {
    id?: string;
    uri?: string;
    type?: string;
    active?: boolean;
    status?: string;
    measures?: Array<Measure>
  }


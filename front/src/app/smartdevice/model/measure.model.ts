export class Measure {
    id?: string;
    key?: string;
    type?: string;
    description?: string;
  }

import { Component, Input } from '@angular/core';
import { FormsModule , FormBuilder, FormGroup, FormArray, Validators  } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SmartDevice } from "../../model/smartdevice.model";
import { SmartDeviceService } from "../../service/smartdevice.service";
import { DeviceService } from "../../service/deviceEngine.service";
import { Measure } from '../../model/measure.model';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';



@Component({
selector: 'ds-device',
templateUrl: './device.component.html'
})
export class DeviceComponent {
     @Input() smartdevice!: SmartDevice;
     measureForm: FormGroup;
     create: boolean = true;
     idExists: boolean = false;

     deviceTypes: string[] = [
         'PowerCounter',
         'Gateway',
         'HeatingZone',
         'HumiditySensor',
         'Light',
         'LightSensor',
         'Nfc',
         'PressureSensor',
         'Shutter',
         'TemperatureSensor',
         'UvSensor'
       ];


   constructor(private fb: FormBuilder, private smartDeviceService: SmartDeviceService , private route: ActivatedRoute, private router: Router, private deviceService: DeviceService )
   {
      this.measureForm = this.fb.group({
         measures: this.fb.array([])
      });

   }

    ngOnInit(): void {
      this.smartdevice = history.state['device'];
      this.create = history.state['create'];
      this.initMeasures();
      // Convertir 'active' en boolean
      if (typeof this.smartdevice.active === 'string') {
         this.smartdevice.active = this.smartdevice.active === 'true';
      }
      this.refreshRaces();
    }

    Save(): void {
        if(this.create == true )
        {
           //this.SaveDeviceTwins();
           this.SaveDevice();
        }
        else {
          this.PutDevice();
        }
    }

     SaveDevice(): void {
           // Create new device
           const measures = this.measureForm.value.measures as Measure[];
           this.smartdevice.measures = measures;

           this.smartDeviceService.PostSmartDevice(this.smartdevice).subscribe(response => {
              console.log(response);
              this.router.navigate(['/device'] , { state: { message: 'Device created successfully!' } });
           });
     }


     PutDevice(): void {
       const measures = this.measureForm.value.measures as Measure[];
       this.smartdevice.measures = measures;

       this.smartDeviceService.PutSmartDevice(this.smartdevice, this.smartdevice.id).subscribe(
          response => {
          this.router.navigate(['/device'] , { state: { message: 'Device update successfully!' } });
          });
     }

     //Measure
     get measures(): FormArray {
         return this.measureForm.get('measures') as FormArray;
       }

       initMeasures(): void {
         if (this.smartdevice?.measures) {
           this.smartdevice.measures.forEach(measure => {
             this.measures.push(this.createMeasureGroup(measure));
           });
         }
       }

       createMeasureGroup(measure?: Measure): FormGroup {
           return this.fb.group({
             key: [measure?.key || '', Validators.required],
             type: [measure?.type || '', Validators.required],
             description: [measure?.description || '', Validators.required]
           });
         }

       addMeasure(): void {
          this.measures.push(this.createMeasureGroup());
       }

       MeasureDelete( index: number ): void {
           this.measures.removeAt(index);
       }

        smartdeviceList: Array<SmartDevice> = [];

        refreshRaces(): void {
           this.smartDeviceService.list().subscribe(listDevices => {
             this.smartdeviceList  = listDevices;
           });
        }

     // Device id check
     checkId() {
       this.idExists = false;
       for (let i = 0; i < this.smartdeviceList.length; i++) {
         if (this.smartdeviceList[i].id === this.smartdevice.id) {
           this.idExists = true;
           break;
         }
       }
     }

}

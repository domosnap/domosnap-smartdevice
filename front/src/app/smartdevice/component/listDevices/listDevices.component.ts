import { Component , OnInit} from '@angular/core';
import { SmartDevice } from "../../model/smartdevice.model";
import { SmartDeviceService } from "../../service/smartdevice.service";
import { Router } from '@angular/router';
import { DeviceService } from "../../service/deviceEngine.service";


@Component({
selector: 'ds-listDevices',
templateUrl: './listDevices.component.html'
})
export class ListDevicesComponent {

    public isWaitingResponse:boolean = true;
    create: boolean = true;
    message: string = "";

    constructor(private service: SmartDeviceService, private router: Router, private deviceService: DeviceService) {
        this.service.list().subscribe(listDevices => {
            // store the array of the races in the component
            this.smartdeviceList  = listDevices;

            this.isWaitingResponse = false;
            });
    }

    ngOnInit(): void {
        this.message = history.state['message'];
        this.refreshRaces();
        this.showMessage();
    }

    showMessage() {

        if (this.message !== "") {
          setTimeout(() => {
            this.hideMessage();
          }, 4000);
        }
      }
      hideMessage() {

        this.message = "";
      }


    currentSmartDevice!: SmartDevice;

    smartdeviceList: Array<SmartDevice> = [];
    refreshRaces(): void {
        //this.smartdeviceList = [{ uri: 'scs://192.1.1.1/11', type:"Light" }, { type: 'Light' }];
        this.service.list().subscribe(listDevices => {
            // store the array of the races in the component
            this.smartdeviceList  = listDevices;
            console.log(this.smartdeviceList);
            this.getStatusLight();
            });

    }

    createDevice(): void {
        const newDevice: SmartDevice = { id:'' , uri: '', type: '', active: false, measures: [] };
        console.log(newDevice);
        this.router.navigate(['/create-device'], { state: { device: newDevice, create: this.create } });
      }

    editDevice(device: SmartDevice): void {
        this.router.navigate(['/create-device'], { state: { device: device , create: this.create = false} });
      }

    onDelete(smartdevice:SmartDevice): void {
        this.service.delete(smartdevice.id).subscribe(response => {
          console.log(response);
             // TODO do only on success!
             // remove the smartdevice
               this.smartdeviceList.forEach((value, index) => {
                   if (value.id == smartdevice.id) {
                       this.smartdeviceList.splice(index, 1);
                   }
              })
        });
    }


    onSelect(smartdevice:SmartDevice) {
        this.currentSmartDevice = smartdevice;
    }

    getStatusLight(): void {

            this.smartdeviceList.forEach(device => {
            console.log("cic");
                const deviceId = device.uri ?? '';
                const encodedUrl = encodeURIComponent(deviceId);
                this.deviceService.getStatusLight(encodedUrl).subscribe(
                    status => {
                        if (device.type && device.type.trim().toLowerCase() === 'light') {
                            const statusString = String(status);
                            device.status = statusString;
                            console.log('Light status:', device.status);
                            const index = this.smartdeviceList.findIndex(item => item.uri === deviceId);
                            if (index !== -1) {
                                this.smartdeviceList[index].status = device.status;
                            }
                        }
                    },
                    error => {
                        device.status = 'Pas de status disponible';
                        const index = this.smartdeviceList.findIndex(item => item.id === device.id);
                        if (index !== -1) {
                          this.smartdeviceList[index].status = device.status;
                        }
                    }
                );
            });
        }

}

import { Component , OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'ds-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private router: Router,private keycloak: KeycloakService) {
    this.keycloak = keycloak;
  }

  ngOnInit() {
      if (window.location.hash.includes('iss=')) {
        window.location.hash = '';
        this.router.navigate(['/']);
      }
  }

  public logout() {
    console.log("logout");
    this.keycloak.logout();
  }
}

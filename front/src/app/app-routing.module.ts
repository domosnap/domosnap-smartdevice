import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/auth/guard/auth.guard';
import { ListDevicesComponent } from './smartdevice/component/listDevices/listDevices.component';
import { DeviceComponent } from './smartdevice/component/device/device.component'
// import { Welcome } from './smartdevice/component/welcome/welcome.component'
import { AppComponent } from './app.component'

const routes: Routes = [
//  { path: '', redirectTo:'home',pathMatch:'full' },
  { path: '', component: ListDevicesComponent , canActivate: [AuthGuard]},
  { path: 'device', component: ListDevicesComponent , canActivate: [AuthGuard]},
  { path: 'create-device', component: DeviceComponent , canActivate: [AuthGuard]}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }

